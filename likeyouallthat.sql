-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: sorting
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classroom`
--

DROP TABLE IF EXISTS `classroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teachId` int(11) DEFAULT NULL,
  `urlId` varchar(30) DEFAULT NULL,
  `cname` varchar(30) DEFAULT NULL,
  `numAdv` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teachId` (`teachId`),
  CONSTRAINT `classroom_ibfk_1` FOREIGN KEY (`teachId`) REFERENCES `teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classroom`
--

LOCK TABLES `classroom` WRITE;
/*!40000 ALTER TABLE `classroom` DISABLE KEYS */;
INSERT INTO `classroom` VALUES (1,NULL,NULL,NULL,NULL),(2,10,'gg32jViSE','test',NULL),(3,10,'t1pcbZ6rr','ge',NULL),(4,11,'QSrqoaJWs','test3',NULL),(5,12,'UY8tZ7EhR','hello',NULL),(6,13,'3fw1TAtkS','fasdf',NULL),(7,13,'TBxS4TAa2','test',NULL),(8,13,'sE4k7TgCo','eetst',NULL),(9,14,'JVkvJ8iEN','etst',NULL),(10,14,'qGxnXcffS','test',NULL),(11,14,'sonAPbJ_Y','test',NULL),(12,14,'1lBr6_Spv','test',NULL),(13,14,'8Qj8UJhss','',NULL),(14,14,'7LMEsOR3u','test',NULL),(15,14,'NrrMZCwOS','test',NULL),(16,14,'0j7m-m-VU','test',NULL),(17,14,'iEI0uc0hK','test',NULL),(18,15,'ixX1KMNuj','test',NULL),(19,15,'-LtC9KOON','test',NULL),(20,15,'Z0XNrdOeU','test',NULL),(21,16,'C1oynShk1','etst',NULL),(22,17,'-dRTQBox9','test',NULL),(23,18,'1WBvXYKhh','test',NULL),(24,19,'E7dWNH_v6','test',NULL),(25,20,'_PY3XiU4y','test',NULL),(26,22,'lDdb1tsAk','test',NULL),(27,3,'j0P_XVhoU','Denmark Community School',NULL),(28,73,'JRLtQ40-o','test3',NULL),(29,85,'4dK86Mfbj','The best calss',NULL),(30,85,'7Rd0hEJCx','The best calss',NULL),(31,85,'Z4UffQm83','The best calss',NULL),(32,85,'-V5N_dSOP','The best calss',NULL),(33,85,'5fp28ZA4R','The best calss',NULL),(34,86,'7NT4jwa_3','The best calss',NULL),(35,87,'51n-aVhNg','The best calss',NULL),(36,88,'YU0O1Wwuq','The best calss',NULL),(37,89,'1T_9K4fLn','The best calss',NULL),(38,90,'QSN4Zp8RS','The best calss',NULL),(39,91,'XK1SnwNoT','The best calss',NULL),(40,92,'1qtSUeobq','The best calss',NULL),(41,93,'wIaG3V5_S','The best calss',NULL),(42,94,'eaAOwpOFi','The best calss',NULL),(43,95,'Es8qajVJq','The best calss',NULL),(44,96,'4k5PINn95','The best calss',NULL),(66,118,'cRyxdVSec','The best calss',NULL),(67,119,'BlK4l5x_O','The best calss',NULL),(68,120,'cgC0gqN4f','The best calss',NULL),(69,121,'Aq2fGgPrl','The best calss',NULL),(70,122,'U5VL_TeYo','The best calss',NULL),(71,123,'0ryFOS6qh','The best calss',NULL),(72,124,'9clQ_EMaS','The best calss',NULL),(73,125,'nxqZ7_toh','The best calss',NULL),(74,126,'ce4MWB4nr','The best calss',NULL);
/*!40000 ALTER TABLE `classroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `house`
--

DROP TABLE IF EXISTS `house`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `house` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hName` varchar(60) NOT NULL,
  `ret_cap` int(11) NOT NULL,
  `ret_current` int(11) NOT NULL,
  `points` int(11) DEFAULT '0',
  `teachId` int(11) DEFAULT NULL,
  `max_num` int(11) DEFAULT NULL,
  `current_adv_one` int(11) DEFAULT '0',
  `current_adv_two` int(11) DEFAULT '0',
  `classId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teachId` (`teachId`),
  KEY `house_ibfk_2` (`classId`),
  CONSTRAINT `house_ibfk_1` FOREIGN KEY (`teachId`) REFERENCES `teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `house_ibfk_2` FOREIGN KEY (`classId`) REFERENCES `classroom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `house`
--

LOCK TABLES `house` WRITE;
/*!40000 ALTER TABLE `house` DISABLE KEYS */;
INSERT INTO `house` VALUES (48,'Honesty',6,6,673,3,8,3,5,NULL),(49,'Integrity',6,6,695,3,7,1,5,NULL),(50,'Respect',6,6,682,3,7,4,3,NULL),(51,'Responsibility',6,6,615,3,7,5,1,NULL),(52,'Trustworthy',6,4,956,3,7,3,4,NULL),(74,'Ravenclaw',4,2,100,1,6,0,0,NULL),(75,'Gryffindor',4,5,100,1,6,0,0,NULL),(76,'Slytherin',4,9,100,1,6,0,0,NULL),(77,'Hufflepuff',4,5,100,1,6,0,0,NULL),(78,'BravenFlaw',4,4,1300,1,6,NULL,NULL,NULL),(79,'Ravenclaw',4,0,100,15,6,0,0,NULL),(80,'Ravenclaw4',4,0,100,15,6,0,0,NULL),(81,'Ravenclaw',4,0,100,16,6,0,0,17),(82,'Ravenclaw',4,0,100,16,6,0,0,17),(83,'Ravenclawzx',4,0,100,16,6,0,0,17),(84,'Ravenclaw',4,0,100,17,6,0,0,NULL),(85,'Ravenclaw',4,0,200,19,6,0,0,NULL),(86,'Ravenclaw',4,0,100,20,6,0,0,NULL),(87,'Ravenclaw',4,0,100,20,6,0,0,NULL),(88,'Ravenclaw',4,0,100,20,6,0,0,25),(89,'Ravenclaw',4,0,100,20,6,0,0,25),(90,'Ravenclaw',4,0,100,20,6,0,0,25),(91,'Ravenclaw',4,0,100,20,6,0,0,25),(92,'Ravenclawfdas',4,0,100,20,6,0,0,25),(93,'Ravenclaw',4,0,100,23,7,NULL,NULL,27),(94,'Ravenclawz',4,0,100,23,6,0,0,27),(95,'Ravenclaw32',4,0,500,23,6,NULL,NULL,27),(96,'Ravenclaw4',4,0,300,23,6,NULL,NULL,27),(97,'Response',4,0,100,118,6,0,0,66),(98,'Res',4,0,100,119,6,NULL,NULL,67),(99,'Res',4,0,100,120,6,NULL,NULL,68),(100,'Res',4,0,100,121,6,NULL,NULL,69),(101,'Res',4,0,100,122,6,NULL,NULL,70),(102,'Res',4,0,100,123,6,NULL,NULL,71),(103,'Res',4,0,100,124,6,NULL,NULL,72),(104,'Res',4,0,100,125,6,NULL,NULL,73),(105,'Res',4,0,100,126,6,NULL,NULL,74);
/*!40000 ALTER TABLE `house` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point`
--

DROP TABLE IF EXISTS `point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `summary` varchar(120) DEFAULT 'No description availble',
  `houseId` int(11) NOT NULL,
  `creator` varchar(120) DEFAULT NULL,
  `add_points` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `classId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `houseId` (`houseId`),
  KEY `point_ibfk_2` (`classId`),
  CONSTRAINT `point_ibfk_1` FOREIGN KEY (`houseId`) REFERENCES `house` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `point_ibfk_2` FOREIGN KEY (`classId`) REFERENCES `classroom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point`
--

LOCK TABLES `point` WRITE;
/*!40000 ALTER TABLE `point` DISABLE KEYS */;
INSERT INTO `point` VALUES (1,'Puzzle',52,'Teacher: dupkem',25,'2018-08-21 16:39:31',27),(2,'Puzzle',50,'Teacher: dupkem',20,'2018-08-21 16:39:53',27),(3,'Puzzle',49,'Teacher: ',15,'2018-08-21 16:56:44',27),(4,'Neshota Scavenger Hunt',49,'Teacher: ',100,'2018-08-23 13:53:49',27),(5,'Neshota Scavenger Hunt',50,'Teacher: ',80,'2018-08-23 13:54:10',27),(6,'Neshota Scavenger Hunt',52,'Teacher: ',60,'2018-08-23 13:54:31',27),(7,'Neshota Scavenger Hunt',48,'Teacher: ',40,'2018-08-23 13:54:50',27),(8,'Neshota Scavenger Hunt',51,'Teacher: ',20,'2018-08-23 13:55:18',27),(9,'Ping Pong Creature',49,'Teacher: ',20,'2018-08-23 14:47:14',27),(10,'Ping Pong Creature',52,'Teacher: ',100,'2018-08-23 15:13:13',27),(11,'Ping Pong Creature',48,'Teacher: ',60,'2018-08-23 15:13:29',27),(12,'Ping Pong Creature',51,'Teacher: ',80,'2018-08-23 15:14:42',27),(13,'Ping Pong Creature',50,'Teacher: ',40,'2018-08-23 15:15:02',27),(14,'NSH Clean Up',52,'Teacher: ',15,'2018-08-23 15:15:35',27),(15,'NSH Clean Up',49,'Teacher: ',15,'2018-08-23 15:16:07',27),(16,'NSH Clean Up',50,'Teacher: ',10,'2018-08-23 15:16:50',27),(17,'Catapult Challenge',49,'Teacher: dupkem',40,'2018-08-23 15:18:23',27),(18,'Catapult Challenge',52,'Teacher: dupkem',100,'2018-08-23 15:18:41',27),(19,'Catapult Challenge',48,'Teacher: dupkem',20,'2018-08-23 15:19:24',27),(20,'Catapult Challenge',50,'Teacher: dupkem',60,'2018-08-23 15:22:03',27),(21,'Catapult Challenge',51,'Teacher: dupkem',80,'2018-08-23 15:22:21',27),(22,'Nature Scavenger Hunt',49,'Teacher: dupkem',23,'2018-08-23 15:23:00',27),(23,'Nature Scavenger Hunt',52,'Teacher: dupkem',33,'2018-08-23 15:23:14',27),(24,'Nature Scavenger Hunt',48,'Teacher: dupkem',19,'2018-08-23 15:23:35',27),(25,'Nature Scavenger Hunt',50,'Teacher: dupkem',13,'2018-08-23 15:23:48',27),(26,'Nature Scavenger Hunt',51,'Teacher: dupkem',11,'2018-08-23 15:24:12',27),(27,'Water Challenge',49,'Teacher: dupkem',40,'2018-08-23 15:24:34',27),(28,'Water Challenge',52,'Teacher: dupkem',50,'2018-08-23 15:24:49',27),(29,'Water Challenge',48,'Teacher: dupkem',30,'2018-08-23 15:25:06',27),(30,'Water Challenge',50,'Teacher: dupkem',20,'2018-08-23 15:25:20',27),(31,'Water Challenge',51,'Teacher: dupkem',10,'2018-08-23 15:26:03',27),(33,'Week 2 Jobs',48,'Teacher: dupkem',23,'2018-09-14 19:59:29',27),(34,'Week 2 Jobs',49,'Teacher: dupkem',18,'2018-09-14 19:59:54',27),(35,'Week 2 Jobs',50,'Teacher: dupkem',30,'2018-09-14 20:02:33',27),(36,'Week Two Jobs',51,'Teacher: dupkem',20,'2018-09-14 20:03:17',27),(37,'Week two Jobs',52,'Teacher: dupkem',22,'2018-09-14 20:03:40',27),(38,'PJ Day',48,'Teacher: dupkem',4,'2018-09-17 14:34:10',27),(39,'PJ Day',49,'Teacher: dupkem',1,'2018-09-17 14:34:19',27),(41,'PJ Day',50,'Teacher: dupkem',5,'2018-09-17 14:35:47',27),(42,'PJ Day',51,'Teacher: dupkem',6,'2018-09-17 14:36:00',27),(43,'PJ Day',52,'Teacher: dupkem',3,'2018-09-17 14:36:11',27),(44,'70s/80s Day',48,'Teacher: dupkem',5,'2018-09-18 13:19:17',27),(45,'70s/80s Day',49,'Teacher: dupkem',1,'2018-09-18 13:19:28',27),(46,'70s/80s Day',50,'Teacher: dupkem',1,'2018-09-18 13:19:39',27),(47,'70s/80s Day',51,'Teacher: dupkem',4,'2018-09-18 13:19:51',27),(48,'70s/80s Day',52,'Teacher: dupkem',2,'2018-09-18 13:20:02',27),(49,'Safari Day',48,'Teacher: dupkem',5,'2018-09-19 13:24:56',27),(50,'Safari Day',49,'Teacher: dupkem',1,'2018-09-19 13:25:12',27),(51,'Safari Day',50,'Teacher: dupkem',1,'2018-09-19 13:25:22',27),(52,'Safari Day',51,'Teacher: dupkem',3,'2018-09-19 13:25:35',27),(53,'Safari Day',52,'Teacher: dupkem',2,'2018-09-19 13:25:46',27),(54,'Hawaiian Day',48,'Teacher: dupkem',4,'2018-09-20 13:18:36',27),(55,'Hawaiian Day',49,'Teacher: dupkem',2,'2018-09-20 13:20:06',27),(56,'Hawaiian Day',50,'Teacher: dupkem',3,'2018-09-20 13:20:28',27),(57,'Hawaiian Day',51,'Teacher: dupkem',4,'2018-09-20 13:20:43',27),(58,'Hawaiian Day',52,'Teacher: dupkem',1,'2018-09-20 13:20:57',27),(59,'Chair Load/Unload',52,'Teacher: dupkem',20,'2018-09-20 13:21:24',27),(60,'Chair load/unload',51,'Teacher: dupkem',5,'2018-09-20 13:21:56',27),(62,'Week 3 Team Activities',49,'Teacher: dupkem',47,'2018-09-20 13:58:09',27),(63,'Week 3 Team Activities',48,'Teacher: dupkem',65,'2018-09-20 13:58:25',27),(64,'Week 3 Team Activities',50,'Teacher: dupkem',62,'2018-09-20 13:59:00',27),(65,'Week 3 Team Activities',51,'Teacher: dupkem',50,'2018-09-20 13:59:11',27),(66,'Week 3 Team Activities',52,'Teacher: dupkem',63,'2018-09-20 13:59:58',27),(67,'Spirit Day',48,'Teacher: dupkem',7,'2018-09-21 14:14:12',27),(68,'Spirit Day',49,'Teacher: dupkem',4,'2018-09-21 14:14:26',27),(69,'Spirit Day',50,'Teacher: dupkem',3,'2018-09-21 14:14:39',27),(70,'Spirit Day',51,'Teacher: dupkem',4,'2018-09-21 14:15:00',27),(71,'Spirit Day',52,'Teacher: dupkem',5,'2018-09-21 14:15:14',27),(72,'House Jobs Week 3',48,'Teacher: dupkem',21,'2018-09-21 19:58:24',27),(73,'House Jobs Week 3',49,'Teacher: dupkem',20,'2018-09-21 19:58:38',27),(74,'House Jobs Week 3',50,'Teacher: dupkem',24,'2018-09-21 19:58:58',27),(75,'House Jobs Week 3',51,'Teacher: dupkem',15,'2018-09-21 19:59:43',27),(76,'House Jobs Week 3',52,'Teacher: dupkem',22,'2018-09-21 20:00:04',27),(78,'Kitchen help!!',52,'Teacher: dupkem',10,'2018-09-24 19:56:04',27),(79,'Float Building',48,'Teacher: dupkem',80,'2018-09-27 13:57:17',27),(80,'Float Building',49,'Teacher: dupkem',60,'2018-09-27 13:57:30',27),(81,'Float Building',50,'Teacher: dupkem',40,'2018-09-27 13:57:41',27),(82,'Float Building',51,'Teacher: dupkem',20,'2018-09-27 13:57:53',27),(83,'Float Building',52,'Teacher: dupkem',100,'2018-09-27 13:58:03',27),(84,'Movie Making',48,'Teacher: dupkem',30,'2018-09-27 13:58:27',27),(85,'Movie Making',49,'Teacher: dupkem',20,'2018-09-27 13:58:37',27),(86,'Movie Making',50,'Teacher: dupkem',21,'2018-09-27 13:58:46',27),(87,'Movie Making',52,'Teacher: dupkem',33,'2018-09-27 13:58:57',27),(88,'Movie Making',51,'Teacher: dupkem',35,'2018-09-27 13:59:14',27),(89,'Thank you notes',51,'Teacher: dupkem',15,'2018-09-27 14:09:00',27),(90,'Activity Article',48,'Teacher: dupkem',20,'2018-09-27 14:09:28',27),(91,'Activity Article',49,'Teacher: dupkem',20,'2018-09-27 14:09:44',27),(92,'Thank you note',48,'Teacher: dupkem',5,'2018-09-27 18:55:05',27),(93,'Thank you note',51,'Teacher: dupkem',5,'2018-09-27 18:55:19',27),(95,'Brewer Pride',48,'Teacher: dupkem',2,'2018-10-02 19:54:39',27),(96,'Brewer Pride',49,'Teacher: dupkem',2,'2018-10-02 19:54:54',27),(97,'Brewer Pride',50,'Teacher: dupkem',1,'2018-10-02 19:55:08',27),(98,'Brewer Pride',51,'Teacher: dupkem',1,'2018-10-02 19:55:18',27),(99,'Brewer Pride',52,'Teacher: dupkem',1,'2018-10-02 19:55:34',27),(100,'House Jobs Week 4',48,'Teacher: dupkem',20,'2018-10-02 19:57:28',27),(101,'House Jobs Week 4',49,'Teacher: dupkem',24,'2018-10-02 19:57:41',27),(102,'House Jobs Week 4',50,'Teacher: dupkem',23,'2018-10-02 19:57:57',27),(103,'House Jobs Week 4',51,'Teacher: dupkem',23,'2018-10-02 19:58:21',27),(104,'House Jobs Week 4',52,'Teacher: dupkem',24,'2018-10-02 19:58:33',27),(105,'Halloween Kahoot',48,'Teacher: dupkem',5,'2018-10-05 13:19:36',27),(106,'Halloween Kahoot',49,'Teacher: dupkem',5,'2018-10-05 13:19:47',27),(107,'Halloween Kahoot',50,'Teacher: dupkem',5,'2018-10-05 13:20:04',27),(109,'Halloween Kahoot',52,'Teacher: dupkem',5,'2018-10-05 13:20:26',27),(110,'House Jobs Week 5',48,'Teacher: dupkem',22,'2018-10-05 19:56:45',27),(111,'House Jobs Week 5',49,'Teacher: dupkem',20,'2018-10-05 19:56:58',27),(112,'House Jobs Week 5',50,'Teacher: dupkem',23,'2018-10-05 19:57:32',27),(113,'House Jobs Week 5',51,'Teacher: dupkem',22,'2018-10-05 19:57:51',27),(114,'House Jobs Week 5',52,'Teacher: dupkem',30,'2018-10-05 19:58:19',27),(115,'Scentsy Sale',52,'Teacher: dupkem',30,'2018-10-18 13:33:58',27),(116,'Scentsy Sale',48,'Teacher: dupkem',20,'2018-10-18 13:34:40',27),(117,'Scentsy Sale',49,'Teacher: dupkem',15,'2018-10-18 13:34:54',27),(118,'Scentsy Sale',50,'Teacher: dupkem',30,'2018-10-18 13:35:15',27),(119,'Scentsy Sale',51,'Teacher: dupkem',15,'2018-10-18 13:35:28',27),(120,'Poster ',50,'Teacher: dupkem',7,'2018-10-18 13:38:30',27),(121,'Poster ',48,'Teacher: dupkem',5,'2018-10-18 13:38:43',27),(122,'Poster ',49,'Teacher: dupkem',3,'2018-10-18 13:39:00',27),(123,'Poster ',51,'Teacher: dupkem',7,'2018-10-18 13:39:13',27),(124,'Poster ',52,'Teacher: dupkem',7,'2018-10-18 13:39:29',27),(125,'Art Room Clean Up',49,'Teacher: dupkem',10,'2018-10-18 13:40:32',27),(126,'Art Room Clean Up',52,'Teacher: dupkem',10,'2018-10-18 13:40:45',27),(127,'Integrity Activities',49,'Teacher: dupkem',10,'2018-10-18 13:41:13',27),(128,'Integrity Activities',48,'Teacher: dupkem',9,'2018-10-18 13:41:28',27),(129,'Integrity Activities',50,'Teacher: dupkem',8,'2018-10-18 13:41:41',27),(130,'Integrity Activities',51,'Teacher: dupkem',7,'2018-10-18 13:41:56',27),(131,'Integrity Activities',52,'Teacher: dupkem',9,'2018-10-18 13:42:08',27),(132,'Password',48,'Teacher: dupkem',11,'2018-10-18 13:43:08',27),(133,'Password',49,'Teacher: dupkem',21,'2018-10-18 13:43:23',27),(134,'Password',50,'Teacher: dupkem',18,'2018-10-18 13:43:38',27),(135,'Password',51,'Teacher: dupkem',20,'2018-10-18 13:43:51',27),(136,'Password',52,'Teacher: dupkem',26,'2018-10-18 13:44:02',27),(137,'Vine Pictionary',50,'Teacher: dupkem',5,'2018-10-18 17:22:20',27),(138,'Vine Pictionary',52,'Teacher: dupkem',4,'2018-10-18 17:22:40',27),(139,'Vine Pictionary',48,'Teacher: dupkem',4,'2018-10-18 17:22:52',27),(140,'Vine Pictionary',49,'Teacher: dupkem',5,'2018-10-18 17:23:03',27),(141,'Vine Pictionary',51,'Teacher: dupkem',3,'2018-10-18 17:23:14',27),(142,'Week 7 House Job',48,'Teacher: dupkem',22,'2018-10-22 19:56:19',27),(143,'Week 7 House Job',49,'Teacher: dupkem',23,'2018-10-22 19:56:35',27),(144,'Week 7 House Job',50,'Teacher: dupkem',24,'2018-10-22 19:56:49',27),(145,'Week 7 House Job',51,'Teacher: dupkem',25,'2018-10-22 19:57:00',27),(146,'Week 7 House Job',52,'Teacher: dupkem',24,'2018-10-22 19:57:12',27),(147,'Devil\'s Lake Food Prep',48,'Teacher: dupkem',10,'2018-10-22 19:57:58',27),(148,'Devil\'s Lake Food Prep',49,'Teacher: dupkem',10,'2018-10-22 19:58:38',27),(149,'Devil\'s Lake Food Prep',48,'Teacher: dupkem',5,'2018-10-22 19:58:56',27),(150,'Devil\'s Lake Food Prep',50,'Teacher: dupkem',5,'2018-10-22 19:59:13',27),(151,'Devil\'s Lake Food Prep',51,'Teacher: dupkem',5,'2018-10-22 19:59:28',27),(152,'Devil\'s Lake Food Prep',52,'Teacher: dupkem',20,'2018-10-22 19:59:46',27);
/*!40000 ALTER TABLE `point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sFName` varchar(60) NOT NULL,
  `sLName` varchar(60) NOT NULL,
  `returning` tinyint(1) DEFAULT '0',
  `houseId` int(11) DEFAULT NULL,
  `teachId` int(11) DEFAULT NULL,
  `advisory_num` int(11) DEFAULT '1',
  `classroomId` int(11) DEFAULT NULL,
  `userhash` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `houseId` (`houseId`),
  KEY `teachId` (`teachId`),
  KEY `classroomId` (`classroomId`),
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`houseId`) REFERENCES `house` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_ibfk_2` FOREIGN KEY (`teachId`) REFERENCES `teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_ibfk_3` FOREIGN KEY (`classroomId`) REFERENCES `classroom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (43,'Mallory','Arrowood',1,50,3,2,1,'',''),(44,'Grace','Blakeslee',1,51,3,2,1,'',''),(45,'William','Arrowood',0,50,3,1,1,'',''),(46,'Tayten','Brunner',1,49,3,2,1,'',''),(48,'Lilly','Genz',0,48,3,1,1,'',''),(49,'Seidl','Herbes',1,48,3,1,1,'',''),(50,'Collin','Babcock',1,50,3,1,1,'',''),(51,'Reilly','Hunter',1,51,3,1,1,'',''),(52,'Anna','Johnson',1,51,3,1,1,'',''),(53,'Kenndra','Kaminski',1,48,3,1,1,'',''),(54,'Tyler','LaPalm',1,52,3,1,1,'',''),(55,'Ryland','Martens',1,NULL,3,1,1,'',''),(56,'Bradley','Nellis',0,52,3,1,1,'',''),(57,'Ethan','Niles',1,51,3,1,1,'',''),(58,'Alexa','Pritzl',1,52,3,1,1,'',''),(59,'Jay','Robinson',1,50,3,1,1,'',''),(60,'Leah','Schilke',1,51,3,1,1,'',''),(61,'Jordan','Stache',1,49,3,1,1,'',''),(62,'Gus','Terrien',1,50,3,1,1,'',''),(63,'Mak','Williquette',1,49,3,1,1,'',''),(64,'Jacob','Zoerb',1,51,3,1,1,'',''),(65,'Dominic','Collard',1,49,3,2,1,'',''),(66,'River','Derenne',1,48,3,2,1,'',''),(68,'Gavin','Glandt',1,48,3,2,1,'',''),(69,'Brianna','Heraly',1,48,3,2,1,'',''),(70,'Brennan','Kramer',1,49,3,2,1,'',''),(71,'Patrick','Mileski',0,52,3,2,1,'',''),(72,'Courtney','O\'Dill',1,49,3,2,1,'',''),(73,'Georgia','Pantzlaff',1,52,3,2,1,'',''),(74,'Macayla','Robinson',0,48,3,2,1,'',''),(75,'Maddie','Salentine',0,52,3,2,1,'',''),(76,'Michael','Schuessler',1,52,3,2,1,'',''),(77,'Nathan','Stewart',1,50,3,2,1,'',''),(78,'Jacob','Veleke',1,48,3,2,1,'',''),(79,'Mara','Weis',1,50,3,2,1,'',''),(80,'Elizabeth','Zoerb',1,49,3,2,1,'',''),(81,'Harry','Potter',1,74,1,0,NULL,'',''),(82,'Harry','Potter',1,NULL,16,0,NULL,'password','hi test'),(83,'Harryasdf','Potter',1,NULL,16,0,NULL,'password','hi test'),(84,'Harrysrsdfgbdxfgbsd','sdrfb',1,NULL,16,0,NULL,'password','hi test'),(85,'Harry','Potter55',1,NULL,16,2,17,'password','hi test');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `summary` varchar(120) DEFAULT 'No summary entered',
  `studId` int(11) NOT NULL,
  `completed` tinyint(1) DEFAULT NULL,
  `comp_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `studId` (`studId`),
  CONSTRAINT `task_ibfk_1` FOREIGN KEY (`studId`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tName` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `userhash` varchar(120) NOT NULL,
  `classroomId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `classroomId` (`classroomId`),
  CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`classroomId`) REFERENCES `classroom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES (1,'Marge','testing@gmail.com','$2b$10$ZHzO2XnU9xKB5k/NebqZAeezTWwp6HcxClWZkEffATj9T/F5psHW.',NULL),(2,'Marge','testin@gmail.com','$2b$10$4CkkERY9tw7zHTS9J9jS8OZi3bZtcS2BGjn3RVCXtxtX9Z0QXsXZm',NULL),(3,'dupkem','dupkem@denmark.k12.wi.us','$2b$10$y00rzeAoYg58MD0YT9sLiOm1yM4FIre/ZFhd16JAUwAyeNHNA9dHG',NULL),(4,'testing','1testing123@gmail.com','$2b$10$LrT0D1aiaF2xG3T0jItynOX5r1x3vO..nQjvPuT6Sr9CUjp8xrlM.',NULL),(5,'Tested ba1','testing23@gmail.com','$2b$10$hyyV.0lVN.tVqb9jk9LFw.Q3iuDWvuh7eovc1y.p.b1Lbgj414n.y',NULL),(6,'testing3','testing35@gmail.com','$2b$10$MixCzeu8InrjYCmXOBGI3uuAweZj/f.pKTbDcITxnNFuqQxDoRIzW',NULL),(7,'testing323','testign334@gmail.com','$2b$10$/.nLpHw5VyApo.1RKEzWpOT.ktv0/y4QvVejZ0MH/QOkxh6q2yCj2',NULL),(8,'testing43@','testing2515@gmail.com','$2b$10$gDUNotGFtJ3WTGXoezwVzOBUlU8BeSUMhGZn31vx1BtjUSwjXhNAy',NULL),(9,'test3414','testing32345@gmail.com','$2b$10$NAN0wIpmoOVAtgCdGYVdG.QOGuZT6vjPsciDfJzz0zz31hhQIht8.',NULL),(10,'tseetsdf','tesas@asdfasdf','$2b$10$1jzLHteYQttp.F5Ri.iZR.w2DMNxBKlUicIg58qBjRsH49VIEh9ES',NULL),(11,'tests','test@asdf','$2b$10$7nkx2m4LVDE.gnPa9zKWOux4CmLOz7coZem2WN2pWbt2WFaejWpam',NULL),(12,'Justin','testing4@gmail.com','$2b$10$S5o7V.4dk1UNhMJdtEkFKOgiTSHk1EAUiQkrN5JBmFI/QrugKq7UO',NULL),(13,'test','tes@asdf','$2b$10$CGx1Qnfihnh6FcooiaadjOpUGwCZFwQwpV..D8180iWO.tZLiXRa2',NULL),(14,'test343','test@asdfa3','$2b$10$5ts.Cykwb2BtOFrfiGbTtu4uveb.OA97pGkTmcVDPNMF5j.XblM9i',NULL),(15,'Justin','asdfaf@gasdlfd','$2b$10$jeB4QajFBpXTYcshNFgf9.eTrRIdE/EypcjU4B25xOURTEwOmNn32',NULL),(16,'testwe','testing@gmail.com34','$2b$10$IGlv8yxtl6m/hYfoXCfFhOFDO2S2HR1tUVFZti1YoQuZ612C3aK1G',NULL),(17,'Justin','test@asdf1234','$2b$10$LPKIvCbp8.Ma1skb0W7ADO7UlySfPBo7BoyT8zmebpOq7KyWYKE3C',NULL),(18,'test','testste@asdf3e3','$2b$10$t.RRfk9JP53PPq5D9gdK5./pRgtFAF8F/NVUEOVkSERFd.qzfh7kC',NULL),(19,'test','asefef@asdf3zxda','$2b$10$qS2we31kMk9/h3D9tL1iMuqqTZ8thy3bdZue0JoOm5KCjRpB0mmuS',NULL),(20,'test3s','testd3@asdf3','$2b$10$S.RZNEe6jcVUT.AMQbVQ/OhDzROAgoAMak6M/eAvr9eP7JyCRvE76',NULL),(21,'Fred','superTestTeach1540861958358','$2b$10$SHf9izF8Qy9VUgR2wJaJGef1n0OOaJQ/ulopOOVX1BIiROq9W672e',NULL),(22,'Justin ','etsdasdf@asdfafxcvx','$2b$10$zEDTkeHcSSt.0wVsoTzg4e4vtYi3vvgwWQmRGIsLCRTdUabGIZDOm',NULL),(23,'test123','asdf@asdf','$2b$10$t18slKvbCjsYUfSoYu/ta.7cgzridOhBjAzRjHZllBkM.lDaNYBAK',NULL),(24,'Fred','superTestTeach1541177251453','$2b$10$rIOk1VW1q4KvQgz/2p1.peyLBAutvonlb3MuNsHVGMRV4UzLr.R2K',NULL),(25,'Fred','superTestTeach1541177861525','$2b$10$8dAKV9w7hn6jKeqp2BNJb.tSv.NHANECRybNh0cyqtlC5QtGhCegG',NULL),(26,'Fred','superTestTeach1541177911646','$2b$10$3TVsbZ36BI8/n8TvbQBao.r4Y/1qkNyL44j6yC.4TeWCHPUvwFrwe',NULL),(27,'Fred','superTestTeach1541177989773','$2b$10$dDO80jcBLnM3j7EK1LZ.DOpGyzyQXlizh1jndDhzjPNctcE51bMkW',NULL),(28,'Fred','superTestTeach1541178145323','$2b$10$FllqY4g/gSTSdRnB6xzzNOZo8lS7AMV/0t2oA72/G4tAaBbKjSAmq',NULL),(29,'Fred','superTestTeach1541178201406','$2b$10$jC.tiU3DA0V6iSlhbLS2LOO8IQtBDZnjtxLrtg6L8S4g.1h4JfGhi',NULL),(30,'Fred','superTestTeach1541178343835','$2b$10$/Wbuq7PQ38FvysVHf4GhjupSpBL1d/lxxYe/9zrruvSa4hjFf8kAe',NULL),(31,'Fred','superTestTeach1541178423343','$2b$10$Qtw0eluDKHTM0a9CXKoLLOlY4bmQamK4I7zM/z7kywyoEcexZCiDy',NULL),(32,'Fred','superTestTeach1541178608433','$2b$10$P3wIBpqP1hAJKR0GL3Mdb.U47mC/FtGNDu5OOHZTJczYkW9QhTikK',NULL),(33,'Fred','superTestTeach1541179779923','$2b$10$0MW.HXQUvxed0Hri40aGbuwT2ttEOCbonpF.AdhEj7re50jTTZtFe',NULL),(34,'Fred','superTestTeach1541180513315','$2b$10$pbR9Zmb2KQuwtglpvs6ituUdchucxQVM47p3jZsPDsxjV1prGmpKu',NULL),(35,'Fred','superTestTeach1541180604021','$2b$10$k7itQCge7LX4Q/sxDghobevFkgUpi3g6yixlEhBPttaOJBNVBYJnG',NULL),(36,'james','superTestTeach1541180646865','$2b$10$XcRCN5wPMb6rpjANBtdBzO6dLj1Hzz/EjgY80b6aJs50k6zLBAYtS',NULL),(37,'james','superTestTeach1541180779861','$2b$10$3sExdNM2LRyW7rJxCQW8hOTokgxicfc8Q6MUnoFtvjzj.cFbCr54G',NULL),(38,'james','superTestTeach1541180850791','$2b$10$yg.o89caciIz65.3Locg1ezptMxqHGXt1xdTmwAdEIulb5ryY.wqa',NULL),(39,'james','superTestTeach1541180877624','$2b$10$0yiobVlTMRVOV.GLgAe4SuVt0mTAzTlUBPMpcvapQFo2ruGZFOfs2',NULL),(40,'james','superTestTeach1541180910568','$2b$10$vOEUwiuYU0gIVj5BvUsafuaF4YDrQRrBANC6wWVHUJF4O/WZBpqjC',NULL),(41,'james','superTestTeach1541180961323','$2b$10$bxan8xCeu8ylxHej4WaPYeEndyEMzkQJuvlIHmaIpNLULy3YflwV.',NULL),(42,'james','superTestTeach1541181025799','$2b$10$32EEtec7klyCIDgqPlKoYuKKsakc3gsuf6L/honpY.9ptemJFlzoK',NULL),(43,'james','superTestTeach1541181061322','$2b$10$i0r39InAC7xp2nYFdt8/nehTavACRpNg9K1saeyfyWJOCr1Y/d9z.',NULL),(44,'james','superTestTeach1541181150482','$2b$10$iWb9K/C2X06CAj3dh1PceechpvCQmuOU3b/grS6FyEhpGqF59zZ6.',NULL),(45,'james','superTestTeach1541181208810','$2b$10$Rf/sS7LeY48t3OMKH2DXxeBXGePSfjwwvYFWqjISG4EEs.S/TAEcq',NULL),(46,'james','superTestTeach1541181242767','$2b$10$L51GqAhUPRxk4spVf2JjY.hm2UohCnBcK754sdAFTqNKQ5KmQk33.',NULL),(47,'james','superTestTeach1541181324384','$2b$10$s9jBUlPzKEo9i.brRDShFuyVLqbQWJbDtRmYT18kWWEeGgonnQbKm',NULL),(73,'testset','asdf2@asdf3','$2b$10$fL3R3gYfcKogFXe0uN8vFuoNzwAvVybH9eCbez/yEG9ZPP.0vdt2q',NULL),(85,'james','superTestTeachfunction now() { [native code] }','$2b$10$C.Y/ecp.vLzyaVg9SVoJGumqcOFThiYNa8BiUCQRVQhd.aOnrLObO',NULL),(86,'james','superTestTeach1541389085002','$2b$10$yYKW1PkaYJ4qziuQsL/16.eRzqdXTzqqeshhnx3.748s1nW7y5mMC',NULL),(87,'james','superTestTeach1541389254878','$2b$10$BeynU1ec9XhpHKfqreVUcOXytNKY4VoH9dWNBxPOljuLTEAKRGgGm',NULL),(88,'james','superTestTeach1541389639417','$2b$10$wN6/SxRS3vNxNm5hHb1ngOALAXueciDfhCVHsBteRtYeWMwGrn1v.',NULL),(89,'james','superTestTeach1541560626958','$2b$10$pwPl3HSCmsO2fXmi8v2s0.3KX5SLvdZO8MMbfCY8wBW6cU.Pve5ZW',NULL),(90,'james','superTestTeach1541732298115','$2b$10$d/9857T6l1t5pbRpEqWdXOLIE1ve6B3z5gNHUfTCkFzR1e/4rq0iu',NULL),(91,'james','superTestTeach1541736787991','$2b$10$X/xh6t2UQlBgTVIjA8Y9UO/rkl4YbiqlcllhQdPTF6A3iZOmVf.vu',NULL),(92,'james','superTestTeach1541736825709','$2b$10$R4YJnwDv3t1fa6vYWAk0euG6Nrv00Te/o5zsCG6nuqIbcUOqwUmT2',NULL),(93,'james','superTestTeach1541737043525','$2b$10$sHxnF4hvu/jYnhBfk15fEO08XCszF1EI7S8QMJpxmb5RJUT0jMEq.',NULL),(94,'james','superTestTeach1541737078403','$2b$10$RzYqoPydZNTa5tVC0d8z/ubugIQphSihN/b0M.mXmEifHdZmM1W2S',NULL),(95,'james','superTestTeach1541737124856','$2b$10$NIByjR7hPq92Do4L6IGG5Ok3fdbzolM0qHtrR53D.yxkNJ8qhFEwe',NULL),(96,'james','superTestTeach1541782377935','$2b$10$/IM/T9gJ313SZ9myKyWXP.VpZSdfrhBSywtAR1S.DUzq1Wsob948m',NULL),(118,'james','superTestTeach1541795306929','$2b$10$nLfeLX1tpujywYrhcyQlXeae1vHeNDC3Ump0PMdDyFlT7QsO2pl9C',NULL),(119,'james','superTestTeach1541795440972','$2b$10$7RFTh7hyunzjV/V5YINQK.cXo7y3M/Sx4Wm0yKHna0L5gNJsALFC.',NULL),(120,'james','superTestTeach1541795480052','$2b$10$i0o4lPITxOZq7XNLfw8I0.SsC5QLWDUoolhD8PMVlezQAKdzDt9V.',NULL),(121,'james','superTestTeach1541795963462','$2b$10$N.ugFRAVVOnYLk.keppdQOK.a831WUl9pAu75PdhfDHe2YLc3BtJm',NULL),(122,'james','superTestTeach1541796743635','$2b$10$Wr5M9Wd1lFnEWCXBuwMOQ.verZovnBbZB7hKbl3BjoXoQyxrjlTLq',NULL),(123,'james','superTestTeach1541796797970','$2b$10$d0YDnasoiwHtyv38gEavZ.6lV.hjlGvmdlnemfR902coPbNsgvaMu',NULL),(124,'james','superTestTeach1541796838756','$2b$10$gcCnW5VFCUasHmc0TLHdDe6/zK1EF6PKHUZqjb0LgbHLP126VpuaS',NULL),(125,'james','superTestTeach1541796849854','$2b$10$WV/nbxTCDsKGDdKd9oFGR.QftnXTfrsuuosZmhUH08buFc.g.i0fi',NULL),(126,'james','superTestTeach1541796873334','$2b$10$dGorOeldL7vbO0zL1l.eqeRQvliaaQCs8q8.fyUCFY/A2WBN3wCH.',NULL);
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-09 16:10:35
