const teachDb = require('../db/teacher_DAO');
const classDb = require('../db/classroom_DAO')
const express = require('express');
const app = module.exports = express();
const router = express.Router();
const bodyParser = require('body-parser');
const Joi = require('joi');
const Author = require('../controllers/auth');
var passportService = require('../services/passport');
const TeacherSchema = {
    tName: Joi.string().alphanum().min(1).max(30),
    username: Joi.string().min(1).max(60),
    returning: Joi.boolean(),
    houseId: Joi.number(),
    teachId: Joi.number(),
    studId: Joi.number()
}
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', {
    session: false
});
const requireSignin = passport.authenticate('local', {
    session: false
});
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);



router.route('/teacher/add')
    .post(function (req, res) {
        let newTeacher = {
            sFName: req.body.fName,
            sLName: req.body.lName,
            returning: req.body.returning,
            houseId: 2,
            teachId: req.body.teachId
        }
        //JOI schema check using promises
        const promise = Joi.validate(newTeacher, TeacherSchema);
        Promise.all([promise, teachDb.insertTeacher(newTeacher)])
            .then((student) => {
                res.json({
                    "student": student[0],
                    "results": student[1]
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/teacher/update')
    .put(requireAuth,function (req, res) {
        let updateTeacher = {
            tName: req.body.tName,
            username: req.body.username,
            teachId: req.user.id,
        }
        //JOI schema check using promises
        const promise = Joi.validate(updateTeacher, TeacherSchema);
        Promise.all([promise, teachDb.updateTeacher(updateTeacher)])
            .then((teacher) => {
                res.json({
                    "teacher": teacher[0],
                    "results": teacher[1]
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/teacher')
    .post(requireAuth, function (req, res) {
        teachDb.getOneTeacher(req.user.id)
            .then((teacher) => {
                res.json({
                    "name": teacher.tName,
                    "email": teacher.username
                })
            })
    });
router.route('/teacher/teach/:id')
    .post(function (req, res) {
        teachDb.getAllTeachers(req.params.id)
            .then((teacher) => {
                res.json({
                    "teacher": teacher
                })
            })
    });
router.route('/teacher/del/')
    .delete(requireAuth,function (req, res) {
        teachDb.deleteTeacher(req.user.id)
            .then((student) => {
                res.json({
                    "student": student,
                    "isDeleted": true
                })
            })
    });

router.route('/teachers/signup')
.post(function( req, res, next){
    Author.signup(req, res, next);
});

router.route('/teachers/signin')
    .post(requireSignin,Author.teacherSignin)