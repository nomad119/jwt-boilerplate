var taskDb = require('../db/task_DAO');
var sortControl = require('../controllers/sort')
var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
var Joi = require('joi');
const passport = require('passport');
const Author = require('../controllers/auth');
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', {
    session: false
});
const TaskSchema = {
    completed: Joi.boolean().required(),
    summary: Joi.string().min(1).max(120),
    comp_date: Joi.date().required(),
    studId: Joi.number().required(),
    id: Joi.number(),
    classroom: Joi.string()
}
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);


router.route('/task/add')
    .post(requireAuth, function (req, res) {
        let newTask = {
            completed: req.body.completed,
            summary: req.body.summary,
            comp_date: req.body.comp_date,
            studId: req.body.studId,
            
        }
        //JOI schema check using promises
        const promise = Joi.validate(newTask, TaskSchema);
        Promise.all([promise, taskDb.insertTask(newTask)])
            .then((task) => {
                res.json({
                    "task": task[0],
                    "results": task[1],
                    "message":"Task created"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/task/update')
    .put(requireAuth,function (req, res) {
        let updateTask = {
            completed: req.body.completed,
            summary: req.body.summary,
            comp_date: req.body.comp_date,
            studId: req.body.studId,
            id : req.body.taskId
        }

        //JOI schema check using promises
        const promise = Joi.validate(updateTask, TaskSchema);
        Promise.all([promise, taskDb.updateTask(updateTask)])
            .then((task) => {
                res.json({
                    "task": task[0],
                    "results": task[1],
                    "message":"Task modified"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

// TODO THE CORRECT ID NEEDS TO BE PASSED TO THESE TWO FUNCTIONS
router.route('/task')
    .post(requireAuth,function (req, res) {
        taskDb.getTaskByStudent(req.body.id)
            .then((student) => {
                res.json({
                    "tasks": student
                })
            })
    });

router.route('/task/del')
    .delete(requireAuth,function (req, res) {
        taskDb.deleteTask(req.body.id)
            .then((task) => {
                res.json({
                    "task": task,
                    "isDeleted": true,
                    "message":"Task deleted"
                })
            })
    });