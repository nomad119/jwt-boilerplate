var express = require('express')
var app = module.exports = express()
const passport = require('passport')
const googleCrtl = require('../controllers/google')
const Author = require('../controllers/auth');
app.use(passport.initialize())

// =====================================
// GOOGLE ROUTES =======================
// =====================================
// send to google to do the authentication
// profile gets us their basic information including their name
// email gets their emails
app.get('/auth/google', passport.authenticate('google', {
  scope: ['profile', 'email', 'https://www.googleapis.com/auth/calendar'],
  session: false,
}));

// the callback after google has authenticated the user
app.get('/auth/google/callback',
  passport.authenticate('google', {
    session: false
  }),
  ((req, res) => {
    console.log('asdfasdfjl;asdfj;klasdf',req.user.token)
    res.cookie('token', req.user.token, { expires: new Date(Date.now() + 900000), httpOnly: false })
    res.redirect(302,'http://localhost:8080')
  })
);
/*
    app.get('/auth/test', ((req,res) =>{
      res.redirect(googleCrtl.url)
    }))
    app.get('/auth/google/callback', ((req, res) =>{
        console.log('queryCODE', req.query.code)
        googleCrtl.tokenGen(req.query.code)
        .then((oauth2Client) => {
          googleCrtl.getCalendarEvents(oauth2Client)
          .then((results) => {
            res.json({'results': results})
          })
        
        })
        .catch((err) =>{
          res.json({"error": err.data})
        })
      })    
    );
    */