var classDb = require('../db/classroom_DAO');
var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
var Joi = require('joi');
var shortid = require('shortid')
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', { session: false });

const classroomSchema = {
    id: Joi.number(),
    classroomName: Joi.string().min(1).max(120),
    urlId: Joi.string(),
    teachId: Joi.number(),
    numOfAdvisory: Joi.number()
}
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);


router.route('/classroom/add')
    .post(requireAuth, function (req, res) {
        let specId = shortid.generate()
        let newclassroom = {
            classroomName: req.body.classroomName,
            teachId: req.user.id,
            urlId: specId,
            numOfAdvisory: req.body.numOfAdvisory
        }
        //JOI schema check using promises
        const promise = Joi.validate(newclassroom, classroomSchema);
        Promise.all([promise, classDb.insertClassroom(newclassroom)])
            .then((classroom) => {
                res.json({
                    "classroom": classroom[0],
                    "results": classroom[1],
                    "message":"classroom created",
                    "id": specId                    
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/classrooms/update')
    .put(requireAuth,function  (req, res) {
        let specId = shortid.generate()
        let updateclassroom = {
            classroomName: req.body.classroomName,
            teachId: req.user.id,
            urlId: specId,
            numOfAdvisory: req.body.numOfAdvisory
        }
        //JOI schema check using promises
        const promise = Joi.validate(updateclassroom, classroomSchema);
        Promise.all([promise, classDb.updateClassroom(updateclassroom)])
            .then((classroom) => {
                res.json({
                    "classroom": classroom[0],
                    "results": classroom[1],
                    "message":"classroom modified"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

// TODO THE CORRECT ID NEEDS TO BE PASSED TO THESE TWO FUNCTIONS
router.route('/classroom:urlId')
    .post(requireAuth,function (req, res) {
        classDb.getClassByTeach(req.body.id)
            .then((classroom) => {
                res.json({
                    "classrooms": classroom
                })
            })
    });

router.route('/classroom/del')
    .delete(requireAuth,function (req, res) {
        classDb.deletesClassroomByUrlId(req.body.urlId)
            .then((classroom) => {
                res.json({
                    "classroom": classroom,
                    "isDeleted": true,
                    "message":"classroom deleted"
                })
            })
    });