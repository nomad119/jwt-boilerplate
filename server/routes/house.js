var houseDb = require('../db/house_DAO');
var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
var Joi = require('joi');
var authController = require('../controllers/auth');
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', { session: false });
const housechema = {
    hName: Joi.string().alphanum().min(1).max(30).required(),
    ret_cap: Joi.number().min(0).required(),
    ret_current: Joi.number().min(0).required(),
    points: Joi.number(),
    teachId: Joi.number().required(),
    houseId: Joi.number(),
    max_num: Joi.number().required(),
    id: Joi.number(),
    classroom: Joi.string()
}
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);


router.route('/houses/add')
    .post(requireAuth, function (req, res) {
        let newHouse = {
            hName: req.body.hName,
            ret_cap: req.body.ret_cap,
            ret_current: req.body.ret_current,
            teachId: req.user.id,
            points: req.body.points,
            max_num: parseInt(req.body.max_num),
            classroom: req.body.classroom
        }
        //JOI schema check using promises
        const promise = Joi.validate(newHouse, housechema);
        Promise.all([promise, houseDb.insertHouse(newHouse)])
            .then((house) => {
                res.json({
                    "house": house[0],
                    "results": house[1],
                    "message":"House created"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/houses/update')
    .put(requireAuth, function (req, res) {
        let updatedHouse = {
            hName: req.body.hName,
            ret_cap: req.body.ret_cap,
            ret_current: req.body.ret_current,
            teachId: req.user.id,
            id: req.body.id,
            points: parseInt(req.body.points),
            max_num: parseInt(req.body.max_num)
            
        }
        //JOI schema check using promises
        const promise = Joi.validate(updatedHouse, housechema);
        Promise.all([promise, houseDb.updateHouse(updatedHouse)])
            .then((house) => {
                res.json({
                    "house": house[0],
                    "results": house[1],
                    "message":"House modified"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/houses/:id')
    .post(function (req, res) {
        houseDb.getOneHouse(req.params.id)
            .then((house) => {
                res.json({
                    "house": house
                })
            })
    });
router.route('/house/teach')
    .post(requireAuth, function (req, res) {
        houseDb.getAllHouses(req.body.classroom)
            .then((house) => {
                res.json({
                    "houses": house
                })
            })
            .catch((error) => {
                res.json({
                  "error":error  
                })
            })
    });
router.route('/house/del')
    .delete(requireAuth, function (req, res) {
        // TODO need to check the user is the one that created it
        houseDb.deleteHouse(req.body.id)
            .then((house) => {
      
            if(!house.error){
                res.json({
                    "message": "House deleted",
                    "isDeleted": true
                })
            } else {
                res.json({
                    "error":"Cannot delete house with students in it.",
                    "isDeleted": false
                })
                }
            })
    });