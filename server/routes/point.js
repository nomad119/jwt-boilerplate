var pointDb = require('../db/point_DAO');
var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
var Joi = require('joi');
var authController = require('../controllers/auth');
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', { session: false });
const pointchema = {
    summary: Joi.string().min(0).max(120),
    creator: Joi.string().min(0).max(120),
    add_points: Joi.number().min(0).required(),
    created: Joi.date(),
    houseId: Joi.number(),
    id: Joi.number(),
    classroom: Joi.string()
}
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);


router.route('/points/add')
    .post(function (req, res) {
        let newPoint = {
            summary: req.body.summary,
            houseId: req.body.houseId,
            creator: req.body.creator,
            add_points: parseInt(req.body.add_points),
            created: req.body.created,
            classroom: req.body.classroom
        }
        console.log('newPOINT FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', newPoint)
        //JOI schema check using promises
        const promise = Joi.validate(newPoint, pointchema);
        Promise.all([promise, pointDb.insertPointAndUpdate(newPoint)])
            .then((point) => {
                res.json({
                    "point": point[0],
                    "results": point[1],
                    "message":"Point created"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/point/update')
    .put(function (req, res) {
        let updatedPoint = {
            summary: req.body.summary,
            houseId: req.body.houseId,
            creator: req.body.creator,
            add_points: parseInt(req.body.add_points),
            created: req.body.created,
            id: req.body.id
            
        }
        //JOI schema check using promises
        const promise = Joi.validate(updatedPoint, pointchema);
        Promise.all([promise, pointDb.updatePoint(updatedPoint)])
            .then((point) => {
                res.json({
                    "point": point[0],
                    "results": point[1],
                    "message":"Point modified"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/point/one')
    .post(function (req, res) {
        pointDb.getOnePoint(req.body.id)
            .then((point) => {
                res.json({
                    "point": point
                })
            })
            .catch((error) => {
                res.json({
                    "error": error
                })
            })
    });
router.route('/points')
    .post(requireAuth,function (req, res) {
        pointDb.getAllPoints(req.body.classroom)
            .then((point) => {
                res.json({
                    "points": point
                })
            })
    });
router.route('/point/del')
    .post( function (req, res) {
        // TODO need to check the user is the one that created it
        pointDb.deletePoint(req.body.id)
            .then((point) => {
                if(!point.error){
                res.json({
                    "message": "Point deleted",
                    "isDeleted": true
                })
            }
            })
            .catch((err) => {
                res.json({
                    "error":"Cannot delete point with students in it.",
                    "isDeleted": false
                })
            })
    });