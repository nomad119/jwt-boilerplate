var studentDb = require('../db/stud_DAO');
var sortControl = require('../controllers/sort')
var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
var Joi = require('joi');
const passport = require('passport');
const Author = require('../controllers/auth');
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', {
    session: false
});
const StudentSchema = {
    sFName: Joi.string().min(1).max(30),
    sLName: Joi.string().min(1).max(30),
    returning: Joi.number().required(),
    houseId: Joi.number().empty().allow(null),
    teachId: Joi.number().required(),
    id: Joi.number(),
    advisory_num: Joi.number(),
    classroom: Joi.string(),
    username: Joi.string(),
    userHash: Joi.string()
}
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);


router.route('/students/add')
    .post(requireAuth, function (req, res) {
        let newStudent = {
            sFName: req.body.sFName,
            sLName: req.body.sLName,
            returning: req.body.returning,
            houseId: req.body.houseId,
            teachId: req.user.id,
            advisory_num: req.body.advisory_num,
            classroom: req.body.classroom,        
            username: req.body.username,
            userHash: req.body.userHash
        }
        //JOI schema check using promises
        const promise = Joi.validate(newStudent, StudentSchema);
        Promise.all([promise, studentDb.insertStudent(newStudent)])
            .then((student) => {
                res.json({
                    "student": student[0],
                    "results": student[1],
                    "message":"Student created"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });

router.route('/students/update')
    .put(requireAuth,function (req, res) {
        let updateStudent = {
            sFName: req.body.sFName,
            sLName: req.body.sLName,
            returning: req.body.returning,
            houseId: req.body.houseId,
            teachId: req.user.id,
            id : req.body.id,
            advisory_num: req.body.advisory_num,
            username: req.body.username,
            userHash: req.body.userHash
        }
        //JOI schema check using promises
        const promise = Joi.validate(updateStudent, StudentSchema);
        Promise.all([promise, studentDb.updateStudent(updateStudent)])
            .then((student) => {
                res.json({
                    "student": student[0],
                    "results": student[1],
                    "message":"Student modified"
                })
            })
            .catch((err) => {
                res.json({
                    "error": err
                });
            });
    });
router.route('/students/sort')
.post(requireAuth, function (req, res) {
    let updateStudent = {
        sFName: req.body.sFName,
        sLName: req.body.sLName,
        returning: req.body.returning,
        houseId: req.body.houseId,
        teachId: req.user.id,
        id : req.body.id,
        advisory_num: req.body.advisory_num,
        username: req.body.username,
        userHash: req.body.userHash
    }
    //JOI schema check using promises
    const promise = Joi.validate(updateStudent, StudentSchema);
    Promise.all([promise, sortControl.sortStudent(updateStudent)])
        .then((student) => {
            res.json({
                "data": student
            })
        })
        .catch((err) => {
            res.json({
                "error": err
            });
        });
});
router.route('/students/:id')
    .post(function (req, res) {
        studentDb.getOneStudent(req.params.id)
            .then((student) => {
                res.json({
                    "student": student
                })
            })
    });
router.route('/student/teach')
    .post(requireAuth, function (req, res) {
        console.log('teacher ID', req.body.classroom)
        studentDb.getAllStudents(req.body.classroom)
            .then((students) => {
                res.json({
                    "students": students
                })
            })
    });
router.route('/student/del')
    .delete(requireAuth,function (req, res) {
        //TODO make sure the teacher that created is the only one that can deleted it.
        studentDb.deleteStudent(req.body.id)
            .then((student) => {
                res.json({
                    "student": student,
                    "isDeleted": true,
                    "message":"Student deleted"
                })
            })
    });
router.route('/student/signup')
.post(function( req, res, next){
    Author.signup(req, res, next);
});

router.route('/students/signin')
    .post(requireSignin,Author.signin);