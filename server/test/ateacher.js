var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var server = require('../index');
chai.use(chaiHttp);
var teacherId = 0;
let teachName = "superTestTeach"+Date.now()
let jwtt = ''
let houseId = ''
let urlId = ''
let pointId = ''
let studentId = ''
let taskId = ''
exports.jwtToken =jwtt;

describe('teacher Calls', () => {
    afterEach(() => {
        server.close();
    });
    it('signs up teacher up', (done) => {
        chai.request(server)
            .post('/api/teachers/signup')
            .send({
                "username": teachName,
                "password": "12345",
                "tName":"Fred"
            })
            .end((err, res) => {
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('token');
                jwtt = res.body.token;
                exports.teacherName = teachName
                done();
            });
    });

    it('signs teacher in', (done) => {
        chai.request(server)
            .post('/api/teachers/signin')
            .send({
                "username": teachName,
                "password": "12345"
            })
            .end((err, res) => {
                if(err)  console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('token');
                jwtt = res.body.token;
                done();
            });
    });
    it('updates a teacher', (done) => {
        chai.request(server)
            .put('/api/teacher/update')
            .set('authorization',jwtt)
            .send({
                "tName": "james",
                "username": teachName
            })
            .end((err, res) => {
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('teacher');
                res.body.teacher.tName.should.equal('james');
                teacherId = res.body.results.insertId;
                done();
            });
    });
    
    it('creates a classroom', (done) =>{
        chai.request(server)
            .post('/api/classroom/add')
            .set('authorization',jwtt)
            .send({
                "classroomName":"The best calss",
                "numOfAdvisory": 2,
            })
            .end( (err, res) =>{
                if (err) console.log('error', err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('classroom');
                classroomId = res.body.results.insertId;
                urlId = res.body.classroom.urlId
                done();
            });
    });
    it('updates a classroom', (done) =>{
    
        chai.request(server)
            .put('/api/classrooms/update')
            .set('authorization',jwtt)
            .send({
              "classroomName":"The second best calss",
              "numOfAdvisory": 2,
             })
            .end( (err, res) =>{
                if (err) console.log('error', err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('classroom');
                res.body.classroom.classroomName.should.equal('The second best calss');
                done();
            });
    });
    it('creates a house', (done) =>{
        chai.request(server)            
            .post('/api/houses/add')
            .set("authorization",jwtt)
            .send({
                "hName":"Response",
                "ret_cap":4,
                "ret_current": 0,
                "points": 100,
                "max_num":6,
                "classroom": urlId
            })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.nested.property('house');
                houseId = res.body.results.insertId;
                done();
            });
    });
    it('updates a house', (done) =>{
        chai.request(server)
            .put('/api/houses/update')
            .set("authorization",jwtt)
            .send({
                "hName":"Res",
                "ret_cap":4,
                "ret_current": 0,
                "points": 100,
                "max_num":6,
                "classroom": urlId,
                "id": houseId
             })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;

                res.body.should.be.a('object');
                res.body.should.have.nested.property('house');
                res.body.house.hName.should.equal('Res');
                done();
            });
    });
    it('get all houses', (done) =>{
        chai.request(server)
            .post('/api/house/teach')
            .set("authorization",jwtt)
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.nested.property('houses');
                done();
            });
    });
        
    it('creates a point', (done) =>{
        chai.request(server)
            .post('/api/points/add')
            .set("authorization",jwtt)
            .send({
            "summary": "first summary for the first point",
            "houseId" :houseId,
            "add_points": 100,
            "creator": "Justin Krause",
            "created": "2018-08-16 18:50:39",
            "classroom": urlId,
            })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.nested.property('point');
                pointId = res.body.results[0].insertId;
                done();
            });
    });
    it('updates a point', (done) =>{
        chai.request(server)
            .put('/api/point/update')
            .set("authorization",jwtt)
            .send({
                "summary": "first summary for the first point",
                "houseId" :houseId,
                "add_points": 200,
                "creator": "Justin",
                "created": "2018-08-16 18:50:55",
                "id": pointId,
                "classroom": urlId,
             })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.nested.property('point');
                res.body.point.creator.should.equal('Justin');
                pointId = res.body.results.insertId;
                done();
            });
    });
    it('get all points', (done) =>{
        chai.request(server)
            .post('/api/points')
            .set("authorization",jwtt)
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.nested.property('points');
                done();
            });
    });
        
    it('creates a student', (done) =>{
        chai.request(server)
            .post('/api/students/add')
            .set("authorization",jwtt)
            .send({
                "sFName":"Test",
                "sLName": "K",
                "houseId": houseId,
                 "returning": 1,
                 "username": 'test',
                 "userHash": 'hi',
                 "advisory_num": 1,
                 "classroom": urlId
            })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.nested.property('student');
                studentId = res.body.results.insertId;
                done();
            });
    });
    it('updates a student', (done) =>{
        chai.request(server)
            .put('/api/students/update')
            .set("authorization",jwtt)
            .send({
                "sFName":"james",
                "sLName": "K",
                 "returning": 0,
                 "username": 'test',
                 "password": 'hi',
                 "studId": studentId,
                 "username": 'test',
                 "userHash": 'hi',
             })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.nested.property('student');
                res.body.student.sFName.should.equal('james');
                done();
            });
    });
    it('get all students', (done) =>{
        chai.request(server)
            .post('/api/student/teach/')
            .set("authorization",jwtt)
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.nested.property('students');
                done();
            });
    });
    it('creates a task', (done) =>{
        chai.request(server)
            .post('/api/task/add')
            .set("authorization",jwtt)
            .send({
                "completed": false,
                "summary": "My first task",
                "studId": studentId,
                "comp_date": "2017-06-15"
            })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');                
                res.body.should.have.nested.property('task');
                res.body.task.summary.should.equal('My first task');
                taskId = res.body.results.insertId;
                done();
            });
    });
    it('updates a tasks', (done) =>{
        chai.request(server)
            .put('/api/task/update')
            .set("authorization",jwtt)
            .send({
                "completed": false,
                "summary": "My second task",
                "studId": studentId,
                "comp_date": "2017-06-15"
             })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.nested.property('task');
                res.body.task.summary.should.equal('My second task');
                done();
            });
    });
    it('get all tasks', (done) =>{
        chai.request(server)
            .post('/api/task')
            .set("authorization",jwtt)
            .send({"id": studentId})
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.nested.property('tasks');
                done();
            });
    });
    it('deletes a tasks', (done) =>{
        chai.request(server)
            .del('/api/task/del/')
            .set("authorization",jwtt)
            .send({"id": taskId})
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('isDeleted');
                res.body.isDeleted.should.have.equal(true);
                done();
            });
    });
    it('deletes a student', (done) =>{
        chai.request(server)
            .del('/api/student/del/')
            .set("authorization",jwtt)
            .send({"id": studentId})
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('isDeleted');
                res.body.isDeleted.should.have.equal(true);
                done();
            });
    });
    it('deletes a point', (done) =>{
        chai.request(server)
            .post('/api/point/del')
            .set("authorization",jwtt)
            .send({
                "id":pointId
            })
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('isDeleted');
                res.body.isDeleted.should.have.equal(true);
                done();
            });
    });
    it('deletes a house', (done) =>{
        chai.request(server)
            .del('/api/house/del/')
            .set("authorization",jwtt)
            .send({"id": houseId})
            .end( (err, res) =>{
                if(err) console.log("Erorr:",err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('isDeleted');
                res.body.isDeleted.should.have.equal(true);
                done();
            });
    });

    it('deletes classroom', (done) =>{
        chai.request(server)
            .del('/api/classroom/del')
            .set("authorization",jwtt)
            .send({
              "urlId" : urlId
             })
            .end( (err, res) =>{
                if (err) console.log('error', err)
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('isDeleted');
                res.body.isDeleted.should.have.equal(true);
                done();
            });
    });

it('deletes a teacher', (done) => {
    chai.request(server)
        .del('/api/teacher/del/')
        .set('authorization',jwtt)
        .end((err, res) => {
            if(err) console.log("Erorr:",err)
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('isDeleted');
            res.body.isDeleted.should.have.equal(true);
            done();
        });
    });
})