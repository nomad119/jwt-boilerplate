var passport = require('passport');
var config = require('../config');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var LocalStrategy = require('passport-local');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
var teachDb = require('../db/teacher_DAO');
var studentDb = require('../db/stud_DAO');
var config = require('../config');
var googleAPIS = require('../controllers/google')
var author = require('../controllers/auth')

var secret = config.secret

// Setup options for JWT Strategy
var jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: secret
};
var localOptions = {
  usernameField: 'username',
  passwordField: 'password'
};

function checkStudentAuth(username, password, done) {
  studentDb.checkStudentPass(username, password)
    .then((studId) => {
      console.log("teachingID", studId)
      if (!studId === false) {
        done(null, studId);
      } else {
        done(null, studId)
      }
    })
    .catch((err) => {

      res.json({
        "error": err
      });
    });
}
// Create local strategy
//TODO Add flag to determine student vs teacher
var localLogin = new LocalStrategy(localOptions, function (username, password, done) {
  //Verify this email and password, call done with the user
  //if it is the correct email and apssword
  //otherwise, call done with false

  teachDb.checkTeacherPass(username, password)
    .then((teachId) => {
      console.log("teachingID", teachId)
      if (!teachId === false || !teachId === undefined) {
        done(null, teachId);
      } else {
        checkStudentAuth(username, password, done)
      }
    })
    .catch((err) => {

      res.json({
        "error": err
      });
    });
});

// Create JWT strategy
var jwtLogin = new JwtStrategy(jwtOptions, function (payload, done) {
  // See if the user ID in the payload exists in our database
  // if it does, call 'done' with that user
  teachDb.getOneTeacher(payload.sub)
    .then((teacher) => {
      done(null, {
        username: teacher.username,
        password: teacher.userhash,
        id: teacher.id
      })
    })
    .catch((err) => {
      res.json({
        "error": err
      });
    });
});
// Tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);
passport.use(new GoogleStrategy({
    clientID: config.googleAuth.clientID,
    clientSecret: config.googleAuth.clientSecret,
    callbackURL: config.googleAuth.callbackURL
  },
  ((tokenSecret, token, refreshToken, profile, done) => {
    let user = {
      sFName: profile.name.givenName,
      sLName: profile.name.familyName,
      email: profile.emails[0].value,
    }
    Promise.all([author.GoogleAuthenication(user),
        googleAPIS.tokenToOAUTH(refreshToken)
      ])
      .then((oauth2Client) => {
        newFunction(oauth2Client)
        console.log('moving aoooooooooooooooooooooooooooooooooooooooooooooooooo')
        done(null, oauth2Client[0])
      })
      .then(() => {

      })
  })
))

function newFunction(oauth2Client) {
  googleAPIS.getCalendarEvents(oauth2Client[1])
    .then((results) => {
      return results;
    });
}
