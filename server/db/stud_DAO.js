var config = require('../config');
var mysql = require('mysql');
// create the connection to database
var pool = mysql.createPool({
  host: config.mysqlHost,
  user: config.mysqlUsername,
  password: config.mysqlPassword,
  database: config.mysqlDatabase
});
  //mysql querys for the students
  const insertStudents           = "INSERT INTO `student`(sFName, sLName, returning, houseId, teachId, advisory_num, username, userhash, classroomId ) Values(?,?,?,?,?,?,?,?,(SELECT classroom.id from `classroom` where classroom.urlId = ?))";
  const insertGoogleAuthStudent  = "INSERT INTO `student` (sFName, sLName, username) Value(?,?,?)"
  const getStudentById           = "SELECT * FROM `student` WHERE id =?";
  const deleteStudent            = "DELETE FROM `student` WHERE id = ?";
  const getStudentByUsername     = "SELECT * FROM `student` WHERE username =?";
  const updatesStudent           = "UPDATE `student`  SET sFname = ?, sLName =?, returning = ?, houseId = ?, teachId= ?, advisory_num= ? WHERE id=?";
  const getStudentsByTeach       = "SELECT  FROM `student` WHERE teachId =?";
  const getStuds                 = "SELECT id, advisory_num, returning,sFName, sLName, IF(houseId IS NULL, 'No House', (SELECT h.hName FROM house as h WHERE h.id = houseId)) as House FROM `student` WHERE classroomId =(SELECT classroom.id from `classroom` where classroom.urlId = ?)"
  const getStudentPass           = "SELECT * FROM `student` WHERE username = ?";
  //Insert student promise based  
exports.insertStudent  = function(newStudent){
  return new Promise( (resolve, reject ) =>{
  pool.query(
    insertStudents, [newStudent.sFName,newStudent.sLName,newStudent.returning,newStudent.houseId,newStudent.teachId, newStudent.advisory_num, newStudent.username, newStudent.userHash,newStudent.classroom], ( error, results, fields ) =>{
      if ( error ) { 
        console.log('Insert studDBError', error.sqlMessage)  
        reject(error.sqlMessage)}
      resolve(results)
  })
});
}
// Insert Google Student
exports.insertStudentGoogle = function(newGoogleStudent){
  console.log('gppgogjkalsjkdfasjdklf',newGoogleStudent,newGoogleStudent.sFName, newGoogleStudent.sLName, newGoogleStudent.email)
  return new Promise((resolve, reject ) => {
    pool.query(
      insertGoogleAuthStudent, [newGoogleStudent.sFName, newGoogleStudent.sLName, newGoogleStudent.email], (error, results, fields) => {
        if ( error ) {
          console.log('insertedGOOGLEUSERERROR', error.sqlMessage)
          reject(error.sqlMessage)
        }
        resolve (results)
       }
    )
  })
}
exports.updateStudent  = function(updatedStudent){
  return new Promise( (resolve, reject) =>{
  pool.query(
    updatesStudent, [updatedStudent.sFName,updatedStudent.sLName,updatedStudent.returning,updatedStudent.houseId,updatedStudent.teachId, updatedStudent.advisory_num, updatedStudent.id], ( error, results, fields ) =>{
      if ( error ) {          
        console.log('Update studDBError', error.sqlMessage)           
        reject(error.sqlMessage)}
      resolve (results)
  });
});
}
exports.getStudentUsername = function (username) {
  return new Promise((resolve, reject) => {
    pool.query(
      getStudentByUsername, [username], (error, results, fields) => {
        if (error) reject(error.sqlMessage);
        if (results) {
          resolve(results[0])
        } else {
          resolve(undefined)
        }
      });
  });
}
exports.getAllStudents  = function(classroomURL){
  return new Promise( (resolve, reject) =>{
  pool.query(
    getStuds, [classroomURL], ( error, results, fields ) =>{
      if ( error ) {          
        console.log('GETTING ALL studDBError', error.sqlMessage)           
        reject(error.sqlMessage)}
      resolve(results);
  });
});
}
exports.getOneStudent  = function(id){
  return new Promise( (resolve , reject) =>{
  pool.query(
    getStudentById, [id], ( error, results, fields ) =>{
      if ( error ) {          
        console.log('GETTING ONE studDBError', error.sqlMessage)           
        reject(error.sqlMessage)}
      if (results[0]){
      resolve(results[0])
      }
  });
});
}
exports.deleteStudent  = function(id){
  return new Promise ( (resolve, reject) =>{
  pool.query(
    deleteStudent, [id], ( error, results, fields ) =>{
      if ( error ) {          
        console.log('DELETING studDBError', error.sqlMessage)           
        reject(error.sqlMessage)}
      resolve(results[0])
  });
});
}
exports.checkStudentPass = function (username, password) {
  return new Promise((resolve, reject) => {
    pool.query(
      getStudentPass, [username],
      function (error, results, fields) {
        if (error) reject(error);
        if (results[0]) {
          var first = results[0];
          bcrypt.compare(password, first.userhash, function (err, isMatch) {
            if (err) console.log(err);
            if (isMatch) {
              resolve(first);
            } else {
              resolve(false);
            }
          });
        } else {
          resolve(undefined);
        }
      }
    )
  })
}
function helperInsertGoogle(newGoogleStudent){
  return new Promise((resolve, reject ) => {
    console.log('new GOOGLE STUDENT COMING IN HOT!!!!!!!!!!!!!!!!', newGoogleStudent)
    pool.query(
      insertGoogleAuthStudent, [newGoogleStudent.sFName, newGoogleStudent.sLName, newGoogleStudent.email], (error, results, fields) => {
        console.log('results from the auth insertion', results)
        if ( error ) {
          console.log('insertedGOOGLEUSERERROR', error.sqlMessage)
          reject(error.sqlMessage)
        }
        resolve (results)
       }
    )
  })
}
exports.checkStudentExistsAndInsert= function(user){
  return new Promise((resolve, reject) => {
    pool.query(
      getStudentPass, [user.username], function (error, results, fields) {
        if (error) {
          console.log('CHECKING EXISTANCE ERROR studDBERROR', error.sqlMessage)
          reject(error.sqlMessage)
        }
        console.log('results from the existing insertion', results)
        if (results){
          resolve (results)
        } else {
          helperInsertGoogle(user)
          .then((results) => {
            resolve(results)
          })
          .catch((err) => {
            reject(err)
          })
        }
      }
    )
  })
}