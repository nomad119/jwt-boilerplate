var config = require('../config');
var mysql = require('mysql');
// create the connection to database

var pool = mysql.createPool({
  host: config.mysqlHost,
  user: config.mysqlUsername,
  password: config.mysqlPassword,
  database: config.mysqlDatabase
});

  //mysql querys for the points
const SQLinsertPoints = "INSERT INTO `point`(summary, houseId, creator, add_points, created)Values(?,?,?,?,CURRENT_TIMESTAMP())";
const SQLgetPointById = "SELECT * FROM `point` WHERE id =?";
const SQLgetAllPointsOrderedASC = "SELECT * FROM `point` ORDER BY created ASC LIMIT 10";
const SQLdeletePoint = "DELETE FROM `point` WHERE id = ?";
const SQLupdatesPointNhouse = "UPDATE `house` SET points= points + ? WHERE id=?";
const SQLPointsWithHouseData = "SELECT p.id, p.summary, p.houseId, p.creator, p.add_points, p.created, h.hName, h.points FROM `point` as p,`house` as h WHERE h.id = p.houseId and p.classId = (SELECT classroom.id from `classroom` where classroom.urlId = ?) ORDER BY created DESC LIMIT 12;"
const SQLPointsWITHDate = "SELECT houseId, add_points as points, created FROM `point` WHERE classId =? ORDER BY houseId"
const SQLupdatesPoint = "UPDATE `point` SET summary=?, houseId =?, creator =?, add_points =?, created =?  WHERE id=?";
//Insert Point promise based  
function insertPoint(newPoint){
  return new Promise( (resolve, reject ) =>{
  pool.query(
    SQLinsertPoints, [newPoint.summary, newPoint.houseId, newPoint.creator, newPoint.add_points], ( error, results, fields ) =>{
      if ( error ) { 
        console.log('insertpointDBError', error.sqlMessage) 
        reject(error.sqlMessage);
      }
      resolve(results)
  })
});
}
exports.insertPointAndUpdate = function(newPoint) {
  return new Promise ((resolve, reject) => {
    Promise.all([insertPoint(newPoint),updatePointsPoint(newPoint.add_points,newPoint.houseId)])
    .then((results) =>{

      resolve(results)
    })
    .catch((error) => {

      reject(error)
    })
  })
}

//UPDATED POINT
exports.updatePoint  = function(updatedPoint){

  return new Promise( (resolve, reject) =>{
  pool.query(
    SQLupdatesPoint, [updatedPoint.summary, updatedPoint.houseId, updatedPoint.creator, updatedPoint.add_points, updatedPoint.created, updatedPoint.id], ( error, results, fields ) =>{

      if ( error ) { 
      console.log('upadtingpointDBError', error.sqlMessage) 
      reject(error.sqlMessage)};

      resolve (results)
  });
});
}
// GET ALL POINTS ORDRED BY TOP 10 ASCENDING TO GET LATEST
exports.getAllPoints  = function(classroomId){
  return new Promise( (resolve, reject) =>{
  pool.query(
    SQLPointsWithHouseData,[classroomId], ( error, results, fields ) =>{
      if ( error ) { 

        console.log('gettingALLPOINTSpointDBError', error.sqlMessage)  
        reject(error.sqlMessage)};

      resolve(results);
  });
});
}
// GET ALL POINTS for data sorting
exports.getAllPointsWithDate  = function(classroomId){
  console.log('classroomid',classroomId)
  return new Promise( (resolve, reject) =>{
  pool.query(
    SQLPointsWITHDate,[classroomId], ( error, results, fields ) =>{
      if ( error ) { 
        console.log('GETALLPOINTSWITHSORTINGpointDBError', error.sqlMessage)  
        reject(error.sqlMessage)};
      resolve(results);
  });
});
}
// get point by id
exports.getOnePoint  = function(id){
  return new Promise( (resolve , reject) =>{
  pool.query(
    SQLgetPointById, [id], ( error, results, fields ) =>{
      if ( error ) { 
        console.log('gettingonepointDBError', error.sqlMessage)  
        reject(error.sqlMessage)};
      if (results){
      resolve(results)
      }
  });
});
}
function updatePointsPoint(add_points,id){
  return new Promise( (resolve , reject) =>{
  pool.query(
    SQLupdatesPointNhouse, [add_points,id], ( error, results, fields ) =>{
      if ( error ) { 
        console.log('updatinggpointDBError', error.sqlMessage) 
        reject(error.sqlMessage);}
      if (results){
      resolve(results)
      }
  });
});
}
exports.deletePoint  = function(id){
  //TODO drop key to null on delete
  return new Promise ( (resolve, reject) =>{
  pool.query(
    SQLdeletePoint, [id], ( error, results, fields ) =>{
      if ( error ) { 
        console.log('deletionpointDBError', error.sqlMessage) 
        resolve({"error":error})
      }
        resolve(results)
        
  });
});
}

