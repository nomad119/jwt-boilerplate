var config = require('../config');
var mysql = require('mysql');
// create the connection to database
var pool = mysql.createPool({
  host: config.mysqlHost,
  user: config.mysqlUsername,
  password: config.mysqlPassword,
  database: config.mysqlDatabase
});
var bcrypt = require('bcrypt');

//compare password with hash
function checkPasswords(candiatePassword, password) {
  bcrypt.compare(password, candiatePassword, function (err, isMatch) {
    if (err) console.log(err);
    return isMatch;

  });
}
//mysql querys for the teacher
const insertTeachers = "INSERT INTO `teacher`(tName,username,userhash) Values(?,?,?)";
const getTeacherById = "SELECT * FROM `teacher` WHERE id =?";
const getTeacherByUsername = "SELECT * FROM `teacher` WHERE username =?";
const deleteTeacher = "DELETE FROM `teacher` WHERE id = ?";
const updateTeacherName = "UPDATE `teacher`  SET tName =?, username = ? WHERE id=?";
const updateTeacherPassword = "UPDATE `teacher`  SET userhash = ? WHERE id=?";
const getTeacherPass = "SELECT * FROM `teacher` WHERE username = ?";

//Insert teacher promise based  
exports.insertTeacher = function (tName,username, password) {
  return new Promise((resolve, reject) => {
    pool.query(
      insertTeachers, [tName,username, password], (error, results, fields) => {
        if ( error ) {          
          console.log('teacherDBError', error.sqlMessage)          
          reject(error.sqlMessage);       
      }
        resolve(results)
      })
  });
}
exports.updateTeacher = function (updatedTeacher) {
  return new Promise((resolve, reject) => {
    pool.query(
      updateTeacherName, [updatedTeacher.tName, updatedTeacher.username, updatedTeacher.teachId], (error, results, fields) => {
        if ( error ) {          
          console.log('teacherDBError', error.sqlMessage)          
          reject(error.sqlMessage);       
      }
        resolve(results)
      });
  });
}
exports.getAllTeachers = function (teachId) {
  return new Promise((resolve, reject) => {
    pool.query(
      getTeacherById, [teachId], (error, results, fields) => {
        if ( error ) {          
          console.log('teacherDBError', error.sqlMessage)          
          reject(error.sqlMessage);       
      }
        resolve(results);
      });
  });
}
exports.getOneTeacher = function (id) {
  return new Promise((resolve, reject) => {
    pool.query(
      getTeacherById, [id], (error, results, fields) => {
        if ( error ) {          
          console.log('teacherDBError', error.sqlMessage)          
          reject(error.sqlMessage);       
      }
        if (results[0]) {
          resolve(results[0])
        }
      });
  });
}
exports.getTeacherUsername = function (username) {
  return new Promise((resolve, reject) => {
    pool.query(
      getTeacherByUsername, [username], (error, results, fields) => {
        if ( error ) {          
          console.log('teacherDBError', error.sqlMessage)          
          reject(error.sqlMessage);       
      }
        if (results[0]) {

          resolve(results[0])
        } else {
          resolve(undefined)
        }
      });
  });
}
exports.deleteTeacher = function (id) {
  return new Promise((resolve, reject) => {
    pool.query(
      deleteTeacher, [id], (error, results, fields) => {
        if ( error ) {          
          console.log('teacherDBError', error.sqlMessage)          
          reject(error.sqlMessage);       
      }
        resolve(results[0])
      });
  });
}
exports.checkTeacherPass = function (username, password) {
  return new Promise((resolve, reject) => {
    pool.query(
      getTeacherPass, [username],
      function (error, results, fields) {
        if (error) reject(error);
        if (results[0]) {

          var first = results[0];
          bcrypt.compare(password, first.userhash, function (err, isMatch) {
            if (err) console.log(err);
            if (isMatch) {
              resolve(first);
            } else {
              resolve(false);
            }

          });

        }
        else{
          resolve(undefined);
        }
      }
    )
  })
}