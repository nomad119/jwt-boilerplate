var config = require('../config');
var mysql = require('mysql');
// create the connection to database

var pool = mysql.createPool({
  host: config.mysqlHost,
  user: config.mysqlUsername,
  password: config.mysqlPassword,
  database: config.mysqlDatabase
});


//mysql querys for the houses
const SQLinsertHouses         = "INSERT INTO `house`(hName,ret_cap,ret_current,points,max_num,teachId, classId)Values(?,?,?,?,?,?,(SELECT classroom.id from `classroom` where classroom.urlId = ?))";
const SQLgetHouseById         = "SELECT * FROM `house` WHERE id =?";
const SQLgetAllHousesByTeach  = "SELECT * FROM `house` WHERE classId = (SELECT classroom.id from `classroom` where classroom.urlId = ?)";
const SQLdeleteHouse          = "DELETE FROM `house` WHERE id = ?";
const SQLupdatesHouse         = "UPDATE `house`  SET hName = ?, ret_cap= ?, ret_current=?,  points=?, max_num=?, current_adv_one=?, current_adv_two =?,teachId=? WHERE id=?";
const SQLupdatePoints         = "UPDATE `house` SET points = ? WHERE id=?";

//Insert House promise based  
exports.insertHouse = function (newHouse) {
  return new Promise((resolve, reject) => {
    pool.query(
      SQLinsertHouses, [newHouse.hName, newHouse.ret_cap, newHouse.ret_current, newHouse.points, newHouse.max_num, newHouse.teachId, newHouse.classroom], (error, results, fields) => {
        if (error) {
          console.log('creatinghouseDBError', error.sqlMessage)
          reject("House was not inserted properly")
        }
        resolve(results)
      })
  });
}
// Updates house
exports.updateHouse = function (updatedHouse) {
  return new Promise((resolve, reject) => {
    pool.query(
      SQLupdatesHouse, [updatedHouse.hName, updatedHouse.ret_cap, updatedHouse.ret_current, updatedHouse.points, updatedHouse.max_num, updatedHouse.current_adv_one, updatedHouse.current_adv_two, updatedHouse.teachId, updatedHouse.id], (error, results, fields) => {
        if (error) {
          console.log('updatinghouseDBError', error.sqlMessage)
          reject("House was not updated properly")
        }
        resolve(results)
      });
  });
}

//Retreives all houses by classroomURL
exports.getAllHouses = function (classroomURL) {
  return new Promise((resolve, reject) => {
    pool.query(
      SQLgetAllHousesByTeach, [classroomURL], (error, results, fields) => {
        if (error) {
          console.log('gettingallhouseDBError', error.sqlMessage)
          reject("Cannot get all houses for you.")
        }
        resolve(results);
      });
  });
}

// Gets one house
exports.getOneHouse = function (id) {
  return new Promise((resolve, reject) => {
    pool.query(
      SQLgetHouseById, [id], (error, results, fields) => {
        if (error) {
          console.log('gettingonehouseDBError', error.sqlMessage)
          reject("Cannot retrieve the house")
        }
        if (results[0]) {
          resolve(results[0])
        }
      });
  });
}
// Updates house points
exports.updatePointsHouse = function (points, id) {
  return new Promise((resolve, reject) => {
    pool.query(
      SQLupdatePoints, [points, id], (error, results, fields) => {
        if (error) {
          console.log('upadtingpointshouseDBError', error.sqlMessage)
          reject("House points were not added.")
        }
        if (results[0]) {
          resolve(results[0])
        }
      });
  });
}
// Deletes house
exports.deleteHouse = function (id) {
  //TODO drop key to null on delete
  return new Promise((resolve, reject) => {
    pool.query(
      SQLdeleteHouse, [id], (error, results, fields) => {
        if (error) {
          console.log('deletinghouseDBError', error.sqlMessage)
          resolve({
            "error": 'Cannot delete house with students in it.'
          })
        }
        resolve(results)
      });
  });
}