var config = require('../config');
var mysql = require('mysql');

// create the connection to database
var pool = mysql.createPool({
  host: config.mysqlHost,
  user: config.mysqlUsername,
  password: config.mysqlPassword,
  database: config.mysqlDatabase
});
 
  //mysql querys for the students
  const insertTasks = "INSERT INTO `task`(summary, studId, completed, comp_date)Values(?,?,?,?)";
  const getTasksByStudentId = "SELECT * FROM `task` WHERE studId = ?";
  const deleteTasks = "DELETE FROM `task` WHERE id = ?";
  const updateTasks = "UPDATE `task`  SET summary = ?, studId = ?, completed = ?, comp_date = ? WHERE id = ?";

//Insert student promise based  
exports.insertTask  = function(newTask){
  return new Promise( (resolve, reject ) =>{
  pool.query(
    insertTasks, [newTask.summary,newTask.studId,newTask.completed,newTask.comp_date], ( error, results, fields ) =>{
      if ( error ) {          
      console.log('Insert a taskDBError', error.sqlMessage)          
      reject(error.sqlMessage);       
    }
      resolve(results)
  })
});
}
// Get all tasks a student has created.
exports.getTaskByStudent  = function(studentId){
  return new Promise( (resolve , reject) =>{
  pool.query(
    getTasksByStudentId, [studentId], ( error, results, fields ) =>{
      if ( error ) {          
        console.log('get a taskDBError', error.sqlMessage)          
      reject(error.sqlMessage);       
    }
      if (results[0]){
      resolve(results[0])
      }
  });
});
}
//Delete task
exports.deleteTask  = function(taskId){
  return new Promise ( (resolve, reject) =>{
  pool.query(
    deleteTasks, [taskId], ( error, results, fields ) =>{
      if ( error ) {          
        console.log('delete a taskDBError', error.sqlMessage)          
        reject(error.sqlMessage);       
    }
      resolve(results[0])
  });
});
}
//update tasks
exports.updateTask  = function(updateTask){
    return new Promise( (resolve, reject) =>{
    pool.query(
      updateTasks, [updateTask.summary,updateTask.studId,updateTask.completed,updateTask.comp_date, updateTask.id], ( error, results, fields ) =>{
        if ( error ) {          
        console.log('update a taskDBError', error.sqlMessage)          
        reject(error.sqlMessage);       
      }
        resolve (results)
    });
  });
  }