var config = require('../config');
var mysql = require('mysql');

// create the connection to database
var pool = mysql.createPool({
  host: config.mysqlHost,
  user: config.mysqlUsername,
  password: config.mysqlPassword,
  database: config.mysqlDatabase
});
//mysql querys for the students
const insertsClassroom  = "INSERT INTO `classroom`(cname, teachId, urlId, numAdv )Values(?,?,?,?)";
const getClassesByTeach = "SELECT * FROM `classroom` WHERE teachId = ?";
const deletesClassroom  = "DELETE FROM `classroom` WHERE id = ?";
const updatesClassroom  = "UPDATE `classroom`  SET cname = ?, teachId = ?, urlId = ?, numAdv =? WHERE id = ?";
const getClassroomId    = "SELECT id from `classroom` WHERE urlId =?"
//Insert student promise based  
exports.insertClassroom = function (newClass) {
  return new Promise((resolve, reject) => {
    pool.query(
      insertsClassroom, [newClass.classroomName, newClass.teachId, newClass.urlId, newClass.numAdv], (error, results, fields) => {
        if (error) {
          console.log('ADDINGclassroomDBError', error.sqlMessage)
          reject(error.sqlMessage);
        }
        resolve(results)
      })
  });
}
// Get all tasks a student has created.
exports.getClassByTeach = function (teachId) {
  return new Promise((resolve, reject) => {
    pool.query(
      getClassesByTeach, [teachId], (error, results, fields) => {
        if (error) {
          console.log('GETTINGCLASSROMclassroomDBError', error.sqlMessage)
          reject(error.sqlMessage);
        }
        if (results) {
          resolve(results)
        }
      });
  });
}
exports.getClassroomId = function (urlId) {
  console.log('urlId')
  return new Promise((resolve, reject) => {
    pool.query(
      getClassroomId, [urlId], (error, results, fields) => {
        if (error) {
          console.log('GETINGLClASSROMclassroomDBError', error.sqlMessage)
          reject(error.sqlMessage);
        }
        if (results) {
          resolve(results[0].id)
        }
      }
    )
  })
}
//Delete task
function deleteClassroom(classroomId) {
  return new Promise((resolve, reject) => {
    pool.query(
      deletesClassroom, [classroomId], (error, results, fields) => {
        if (error) {
          console.log('DELETEINGclassroomDBError', error.sqlMessage)
          reject(error.sqlMessage);
        }
        resolve(results)
      });
  });
}
exports.deletesClassroomByUrlId = function (urlId) {
  return new Promise((resolve, reject) => {
    pool.query(
      getClassroomId, [urlId], (error, results, fields) => {
        if (error) {
          console.log('DELETINGclassroomDBError', error.sqlMessage)
          reject(error.sqlMessage)
        }
        deleteClassroom(results[0].id)
          .then((results) => {
            resolve(results)
          })
          .catch((error) => {
            reject(error)
          })
      })
  })
}
//update tasks
exports.updateClassroom = function (updatedClass) {
  return new Promise((resolve, reject) => {
    pool.query(
      updatesClassroom, [updatedClass.name, updatedClass.teachId, updatedClass.urlId, updatedClass.numAdv, updatedClass.id], (error, results, fields) => {
        if (error) {
          console.log('UDPDATINGclassroomDBError', error.sqlMessage)
          reject(error.sqlMessage);
        }
        resolve(results)
      });
  });
}