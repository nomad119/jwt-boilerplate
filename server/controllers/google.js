const {google} = require('googleapis')
const {googleAuth} = require('../config')
const oauth2Client = new google.auth.OAuth2(
    googleAuth.clientID,
    googleAuth.clientSecret, 
    googleAuth.callbackURL
)

const scopes = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/calendar'
]

exports.url = oauth2Client.generateAuthUrl({
  // 'online' (default) or 'offline' (gets refresh_token)
  access_type: 'offline',
 
  // If you only need one scope you can pass it as a string
  scope: scopes
});

// Takes query code from request and 
exports.queryToOAUTH = function(query){
  return new Promise ((resolve, reject) => {
    oauth2Client.getToken(query)
    .then((results) =>{
      oauth2Client.credentials = results.tokens
      resolve(oauth2Client)
    })
    .catch((err) =>{
      console.log('errrorrror', err.data)
      reject(err)
    })
  })
}
exports.tokenToOAUTH = function(tokens){
    return new Promise((resolve, reject) => {
      oauth2Client.credentials = tokens
      resolve(oauth2Client)
    })
    .catch((err) =>{
      console.log('errrorrror', err.data)
      reject(err)
    })
}
exports.getUserInfo = function(OAuth) {
  return new Promise((resolve, reject) =>{
  })
}
exports.getCalendarEvents = function (voauth2Client){
  return new Promise((resolve, reject) => {
    const calendar = google.calendar({version: 'v3', auth:voauth2Client})
    calendar.events.list({
      calendarId: 'primary',
      timeMin: (new Date()).toISOString(),
      maxResults: 10,
      singleEvents: true,
      orderBy: 'startTime',
    }, (err, res) => {
      if (err) return console.log('The API returned an error: ' + err);
      const events = res.data.items;
      if (events.length) {
        console.log('Upcoming 10 events:');
        let evented = events.map((event, i) => {
          const start = event.start.dateTime || event.start.date;
          console.log(`${start} - ${event.summary}`);
          return (`${start} - ${event.summary}`)
        });
        resolve(evented)
  
      } else {
        console.log('No upcoming events found.');
        reject(err)
      }
    })
  })
}
exports.createNewEvent = function (uOAUTH, nEvent) {
  return new Promise((resolve, reject) => {
    const calendar = google.calendar({version: 'v3', auth:voauth2Client})
    calendar.events.insert({
      auth: uOAUTH,
      calendarId: 'primary',
      resource: nEvent
    }, function(err, event) {
      if (err) {
        reject(err)
      }
      resolve("Event Created", event)
    })
  })
}