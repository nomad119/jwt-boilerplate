function updateStudentNHouse(sortingStudent, selectedHouse){
  return new Promise((resolve,reject) => {
    sStudent.houseId = sHouse.id
    if (sortingStudent.advisory_num === 2) {
      sHouse.current_adv_two++
    } else if (sortingStudent.advisory_num === 1) {
      sHouse.current_adv_one++
    }
    Promise.all([studentDb.updateStudent(sStudent), houseDb.updateHouse(sHouse)])
    .then((data) => {
      resolve(data)
    })
    .catch((err) => reject(err))
  })
}
function houseValidation(sortingStudent, houses){
  return new Promise((resolve,reject) =>{
    let randomHouseIndex = getRandomInt(houses.length)
    let selectedHouse = houses[randomHouseIndex]
    let successStudent ={}
    if (selectedHouse === undefined){
      reject('Houses are full')
    }
    while(houses.length >0){
      if (selectedHouse.max_num > selectedHouse.current_adv_one + selectedHouse.current_adv_two 
        && (sStudent.advisory_num < Math.ceil(sHouse.max_num / 2))){
        if (sortingStudent.returning  && (sHouse.ret_cap > sHouse.ret_current) ){
          //House has room update ret current and advisory numbers
          sHouse.ret_current++
          updateStudentNHouse(sortingStudent,selectedHouse)
          .then((updatedData) =>{
            resolve(updatedData)
          })
          .catch((err) => reject(err))
        } 
        if (!sortingStudent.returning){
          updateStudentNHouse(sortingStudent,selectedHouse)
          .then((updatedData) =>{
            resolve(updatedData)
          })
          .catch((err) => reject(err))
        }
      }
      //termination && loop driving condition 
      houses.splice(randomHouseIndex,1)
      randomHouseIndex = getRandomInt(houses.length)
      selectedHouse = houses[randomHouseIndex]
      }
      reject('Houses are full')
  })
}