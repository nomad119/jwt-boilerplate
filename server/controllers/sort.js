const config = require('../config')
const teacherDb = require('../db/teacher_DAO')
const houseDb = require('../db/house_DAO')
const studentDb = require('../db/stud_DAO')
const filtStudent = config.students

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max))
}
function checkStudent(sortingStudent, houses){
  return new Promise((resolve,reject) =>{
  let filterDBHouses = []
console.log('sortStudent', sortingStudent.teachId)
  studentDb.getAllStudents(sortingStudent.teachId)
  .then((filterDBStudents) =>{
  console.log('db students ', filterDBHouses)
  for (let i =0; i < filtStudent.length; i++){
    //Check to see if the student is on the list by looking at their first and last name
    if (filtStudent[i].sFName === sortingStudent.sFName && filtStudent[i].sLName === sortingStudent.sLName){
      // Iterate over student filters
       console.log('sortStudent', filtStudent[i])
       for (let j = 0; j < filtStudent[i].sFilter.length; j++){
         console.log('sortingFiltered', filtStudent[i].sFilter[j])
         // Iterate students in the database
          for (let k = 0; k < filterDBStudents.length; k++){
            console.log('filtStudent',filtStudent[i].sFilter[j].sFName, 'db', filterDBStudents[k].sFName)
            // Check to see if existing sorted students are parted of the filtered list and splice the house from the selection
            if (filtStudent[i].sFilter[j].sFName === filterDBStudents[k].sFName && filtStudent[i].sFilter[j].sLName === filterDBStudents[k].sLName) {
              // filter
              for (let l =0; l < houses.length; l++){
                if (houses[l].id === filterDBStudents[k].houseId){
                  houses.splice(randomHouseIndex,1)
                }
              }
              console.log('housesffffffffff', houses)

            }
          }
       }
    }

  }
  resolve(houses)
  })
})
}
function updateStudentNHouse(sortingStudent, selectedHouse){
  return new Promise((resolve,reject) => {
    sortingStudent.houseId = selectedHouse.id
    if (sortingStudent.advisory_num === 2) {
      selectedHouse.current_adv_two++
    } else if (sortingStudent.advisory_num === 1) {
      selectedHouse.current_adv_one++
    }
    Promise.all([studentDb.updateStudent(sortingStudent), houseDb.updateHouse(selectedHouse)])
    .then((data) => {
      resolve(data)
    })
    .catch((err) => reject(err))
  })
}
function houseValidation(sortingStudent, houses){
  return new Promise((resolve,reject) =>{
    let isSorted = false
    let isAtCap = true
    let isAtAdvCap = true
    let isAtRetCap = true
    let filtHouses = []
    let randomHouseIndex = getRandomInt(houses.length)
    let selectedHouse = houses[randomHouseIndex]
    let current_adv = selectedHouse.current_adv_one
    if (sortingStudent.advisory_num === 2) {
      current_adv = selectedHouse.current_adv_two
    }
    if (selectedHouse === undefined){
      reject('Houses are full')
    }
    checkStudent(sortingStudent, houses)
    .then((filteredHouses) =>{
      filtHouses = filteredHouses
    })
    console.log('filtering houses', filtHouses)
    while(filtHouses.length >0){
      isSorted = false
      isAtCap = true
      isAtAdvCap = true
      isAtRetCap = true
      current_adv = selectedHouse.current_adv_one
      if (sortingStudent.advisory_num === 2) {
        current_adv = selectedHouse.current_adv_two
      }
      // Check if the house overall full and if the max advisory cap is full
      if ((selectedHouse.max_num > selectedHouse.current_adv_one + selectedHouse.current_adv_two )) {
        //Disable error Boolean for string building 
        isAtCap = false
        if (current_adv < Math.floor(selectedHouse.max_num / 2)) {
          isAtAdvCap = false
          //Check returning student
          if (sortingStudent.returning  && (selectedHouse.ret_cap > selectedHouse.ret_current) ){
            isAtRetCap = false
            //House has room update ret current and advisory numbers
            isSorted = true
            selectedHouse.ret_current++
            break
          } 
          if (!sortingStudent.returning){
            isSorted =true
            break
          }
        }
      }
      //termination && loop driving condition 
      houses.splice(randomHouseIndex,1)
      randomHouseIndex = getRandomInt(houses.length)
      selectedHouse = houses[randomHouseIndex]
      
      }
      if(isSorted){
        updateStudentNHouse(sortingStudent,selectedHouse)
        .then(() =>{
          resolve({selectedHouse})
        })
        .catch((err) => reject(err))
      } else {
        let errorMessage={}
        if (isAtCap) errorMessage.max ='The houses are full'
        if (isAtRetCap) errorMessage.ret= 'The houses are full of returners'
        if (isAtAdvCap) errorMessage.advisory= `Advisory: ${sortingStudent.advisory_num} is full in all houses`
        reject(errorMessage)
      }
  })
}

exports.sortStudent = function (stud) {
  return new Promise((resolve, reject) => {
    houseDb.getAllHouses(stud.teachId)
      .then((houses) => {
        houseValidation(stud, houses)
          .then((data) => {
            resolve(data)
          })
          .catch((err) => {
            reject(err)
          })
      })
  })
}

