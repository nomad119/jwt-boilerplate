const config = require('../config')
const teacherDb = require('../db/teacher_DAO')
const houseDb = require('../db/house_DAO')
const studentDb = require('../db/stud_DAO')

function getRandomInt(max, prev) {
  let newChoice = []
  for(let i = 0; i< max; i++){
    newChoice[i] = i
  }
  const prevIndex = newChoice.indexOf(prev)
  if (prevIndex !== -1) {
    newChoice.splice(prevIndex,1)
  }
  const choiceIndex =Math.floor(Math.random() * Math.floor(newChoice.length))

  return newChoice[choiceIndex]
}
// Rules engine for returning student
function returnStudentEngine(adv_num, checkHouse){
  //Check advisory cap and return cap
  if ((adv_num < Math.ceil(selectedHouse.max_num / 2)) && (selectedHouse.ret_cap > selectedHouse.ret_current) ) {
    updateHouseAndStudent(adv_num)
  } else {
    sortAttemptFail(checkHouse)
  }
}
// Rules engine for new student
function newStudentEngine(adv_num, checkHouse){
  //Check advisory cap
  if (adv_num < Math.ceil(selectedHouse.max_num / 2)) {
    updateHouseAndStudent(adv_num)
  } else {
    sortAttemptFail(checkHouse)
  }
}
// Student was succesfully added to house 
// update house and student information on database
function updateHouseAndStudent(adv_num){
  sortingStudent.houseId = selectedHouse.id
  if (adv_num === 2) {
    selectedHouse.current_adv_two++
  } else if (adv_num === 1) {
    selectedHouse.current_adv_one++
  }
  Promise.all([studentDb.updateStudent(sortingStudent), houseDb.updateHouse(selectedHouse)])
  .then((data) => {
    resolve(data)
  })
  .catch((err) => reject(err))
}
//Attempt sort again
function sortAttemptFail(checkHouse, prevId){
  checkHouse++
  if (checkedHouse < houses.length) {
  //Set failed house id to previous to make sure we don't go in there again
  getRandomInt(houses.length, prevId)
  } else {
  reject('houses for returners are full')
  }


function houseValidation(sortingStudent, houses) {
  let isSorted = false
  let checkedHouse = 0
  let previousHouseId = 0
  return new Promise((resolve, reject) => {
    let randomHouseIndex = getRandomInt(houses.length,-1)
    let selectedHouse = houses[randomHouseIndex]
    if (selectedHouse === undefined){
      reject('Houses are full')
    }
    let current_adv = selectedHouse.current_adv_one
    if (sortingStudent.advisory_num === 2) {
      current_adv = selectedHouse.current_adv_two
    }
    // Check to see if the house is full
    if (selectedHouse.max_num > selectedHouse.current_adv_one+ selectedHouse.current_adv_two){

    if (sortingStudent.returning){
      //do this code
      returnStudentEngine(sortingStudent.advisory_num)
    }
    if (!sortingStudent.returning){
      newStudentEngine(sortingStudent.advisory_num)
    }
    }
  }
}

exports.sortStudent = function (stud) {
  return new Promise((resolve, reject) => {
    houseDb.getAllHouses(stud.teachId)
      .then((houses) => {
        houseValidation(stud, houses)
          .then((data) => {
            resolve(data)
          })
          .catch((err) => {
            reject(err)
          })
      })
  })
}



////////////////
const config = require('../config')
const teacherDb = require('../db/teacher_DAO')
const houseDb = require('../db/house_DAO')
const studentDb = require('../db/stud_DAO')

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
function houseValidation(sortingStudent, houses) {
  // Using a while loop until the student is sorted into a house.
  // The two main conditions for house sorting are returning student and which advisory they reside in

  // Initialize local variables for the while loop
  let isSorted = false
  let checkedHouse = 0
  return new Promise((resolve, reject) => {
    let randomHouseIndex = getRandomInt(houses.length)
    let selectedHouse = houses[randomHouseIndex]
    if(selectedHouse === undefined){
      reject('Houses are full')
    }
    let current_adv = selectedHouse.current_adv_one
    if (sortingStudent.advisory_num === 2) {
      current_adv = selectedHouse.current_adv_two
    }
    while (!isSorted) {
      // Check to see if selected house is full.
      if(selectedHouse.max_num > selectedHouse.current_adv_one+ selectedHouse.current_adv_two){
              // Check the house has room for a student from the current advisory
      if (current_adv < Math.ceil(selectedHouse.max_num / 2)) {
        // Check the student is a returning a student && If the house has room for a returning student
        if (sortingStudent.returning) {
          if (selectedHouse.ret_cap > selectedHouse.ret_current) {
            //update the house with the ret_current and advisory cap
            current_adv++
            selectedHouse.ret_current++
              if (sortingStudent.advisory_num === 2) {
                selectedHouse.current_adv_two = current_adv
              } else if (sortingStudent.advisory_num === 1) {
              selectedHouse.current_adv_one = current_adv
            }
            // Update students house Id to be the same as the selected house Id
            sortingStudent.houseId = selectedHouse.id
            //stop the while loop
            isSorted = true
            // update house and student on database with inserted student and house logic
            Promise.all([studentDb.updateStudent(sortingStudent), houseDb.updateHouse(selectedHouse)])
            .then((data) => {
              resolve(data)
              
            })
            .catch((err) => reject(err))
          } 
        } else if (!sortingStudent.returning) {
          // The student is a new student
          sortingStudent.houseId = selectedHouse.id
          current_adv++
            if (sortingStudent.advisory_num === 2) {
              selectedHouse.current_adv_two = current_adv
            } else if (sortingStudent.advisory_num === 1) {
            selectedHouse.current_adv_one = current_adv
          }
          isSorted = true
          Promise.all([studentDb.updateStudent(sortingStudent), houseDb.updateHouse(selectedHouse)])
            .then((data) => {
              resolve(data)
              
            })
            .catch((err) => reject(err))
        }
      } 
    } 
    checkedHouse++
    if (checkedHouse < houses.length && houses.length >1) {
      //Set failed house id to previous to make sure we don't go in there again
      let failedHouseIndex = randomHouseIndex
      houses.splice(failedHouseIndex,1)
      while(randomHouseIndex === failedHouseIndex){
      randomHouseIndex = getRandomInt(houses.length)
      }
      selectedHouse = houses[randomHouseIndex]
    } else {
      reject('houses for returners are full')
      break
    }
     
    
  }
  })
}
exports.sortStudent = function (stud) {
  return new Promise((resolve, reject) => {
    houseDb.getAllHouses(stud.teachId)
      .then((houses) => {
        houseValidation(stud, houses)
          .then((data) => {
            resolve(data)
          })
          .catch((err) => {
            reject(err)
          })
      })
  })
}