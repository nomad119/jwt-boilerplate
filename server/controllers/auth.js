var config = require('../config');
var jwt = require('jwt-simple');
var teacherDb = require('../db/teacher_DAO');
var studentDb = require('../db/stud_DAO');
var classDb = require('../db/classroom_DAO')
var bcrypt = require('bcrypt');
var saltRounds = 10;
var secret = config.secret;
//token generator for user
function tokenForUser(user) {
    var timestamp = new Date().getTime();
    return jwt.encode({
        sub: user,
        iat: timestamp
    }, secret);
}

function checkPasswords(candiatePassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(candiatePassword, password, function (err, isMatch) {
            if (err) reject(err);
            resolve(isMatch);
        });
    });
}

function addTeacher(tName, username, password, res) {
    bcrypt.hash(password, saltRounds, function (err, hash) {
        teacherDb.insertTeacher(tName, username, hash)
            .then((teacher) => {
                res.json({
                    token: tokenForUser(teacher.insertId)
                })
            });
    })
}

function addStudent(student, res) {
    bcrypt.hash(student.password, saltRounds, function (err, hash) {
        student.userhash = hash
        studDb.insertStudent(student)
            .then((stud) => {
                res.json({
                    token: tokenForUser(stud)
                })
            });
    })
}
exports.signup = function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    var tName = req.body.tName;

    if (!username || !password) {
        return res.status(422).send({
            error: 'You must enter both email and password'
        });
    }
    teacherDb.getTeacherUsername(username)
        .then((pteacher) => {
            if (pteacher) {
                return res.status(422).send({
                    error: 'Email is in use'
                });
            } else {
                addTeacher(tName, username, password, res);
            }
        })
        .catch((err) => {
            res.json({
                "error": err
            });
        });

};
// Check to see if the google user already exists in a table
// If not then add them to table
exports.GoogleAuthenication = function (user) {
    return new Promise((resolve, reject) => {
        studentDb.checkStudentExistsAndInsert(user)
            .then((results) => {
                console.log('results from google insertion')
                resolve({
                    token: tokenForUser(results.insertId)
                })
            })
            .catch((err) => {
                reject(console.log(err))
            })
    })
}
exports.signin = function (req, res, next) {
    //User has already had their email and password auth'd
    // we just to give them a token
    res.json({
        name: req.user.tName,
        token: tokenForUser(req.user.id)
    });
};
exports.teacherSignin = function (req, res, next) {
    //User has already had their email and password auth'd
    // we just to give them a token

    classDb.getClassByTeach(req.user.id)
        .then((data) => {
            res.json({
                name: req.user.tName,
                token: tokenForUser(req.user.id),
                class: data
            })
        })
};
exports.studentSignupGoogle = function (user) {
    studentDb.insertStudentGoogle(user)
        .then((results) => {
            return results
        })
        .catch((err) => {
            return err
        })
}