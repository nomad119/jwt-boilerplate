//main starting point of the application

var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var app = express();
var teacher = require('./routes/teacher');
var student = require('./routes/student');
var house = require('./routes/house');
var point = require('./routes/point');
var task = require('./routes/task')
var socket = require('socket.io')
var auth = require('./routes/auth')
var classroom = require('./routes/classroom');
var mongoose = require('mongoose');
var cors = require('cors');

app.use(cors());
//App Setup
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json({
    type: '*/*'
}));

app.use(teacher);
app.use(classroom);
app.use(student);
app.use(house);
app.use(point);
app.use(task)
app.use(auth)

//Server Setup
//check to see if there is a environment process port availbe or 3090
var port = 3090;
console.log('Server listening on:', port);
let server = module.exports = app.listen(port);

let io =  socket(server);

io.on("connection", function(socket){
  console.log("Socket Connection Established with ID :"+ socket.id)
  socket.on("chat", async function(chat){
    chat.created = new Date()
    let response = await new message(chat).save()
    socket.emit("chat",chat)
  })
})