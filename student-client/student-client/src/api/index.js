import Axios from "axios";
import store from "@/store";
const Sorting = "http://locahost:3090";


export function getStudentUsername() {
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      headers: { authorization: localStorage.getItem("token") }
    };
    let data = "";
    Axios.post(`${Sorting}/api/student`, data, axiosConfig)
      .then(({ data }) => {
        store.setUsername(data.name);
        resolve(data);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function getHouse() {
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      headers: { authorization: localStorage.getItem("token") }
    };
    let data = {};

    Axios.post(`${Sorting}/api/student/house`, data, axiosConfig)
      .then(({ data }) => {
        resolve(data.houses);
      })
      .catch(error => {
        reject(console.log(error));
      });
  });
}
// Get all tasks that belong to a student
export function getTasks() {
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      headers: { authorization: localStorage.getItem("token") }
    };
    let data = {};

    Axios.post(`${Sorting}/api/tasks`, data, axiosConfig)
      .then(({ data }) => {
        resolve(store.setTasks(data.tasks));
      })
      .catch(error => {
        reject(console.log(error));
      });
  });
}
// Create new task
export function addTask(newTask) {
  console.log();
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      headers: { authorization: localStorage.getItem("token") }
    };
    let data = newTask;
    Axios.post(`${Sorting}/api/task/add`, data, axiosConfig)
      .then(({ data }) => {
        getTasks();
        resolve(data);
      })
      .catch(error => {
        reject(error);
      });
  });
}
// Delete task
export function deleteStudent(task) {
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      headers: { authorization: localStorage.getItem("token") }
    };
    let data = task;
    Axios.post(`${Sorting}/api/task/del`, data, axiosConfig)
      .then(({ data }) => {
        console.log(data);
        resolve(data);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
}
// Edit tasks
export function editTask(editTask) {
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      headers: { authorization: localStorage.getItem("token") }
    };
    let data = editTask;
    Axios.put(`${Sorting}/api/tasks/update`, data, axiosConfig)
      .then(({ data }) => {
        resolve(data);
      })
      .catch(error => {
        reject(error);
      });
  });
}
export function checkAuth(context) {
  const token = document.cookie
  console.log('hello',context.$cookie.get("token"))
}
