import "babel-polyfill";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Vuetify from "vuetify";
import { store } from "./store"
import "vuetify/dist/vuetify.min.css";
import VueSocketio from "vue-socket.io";
import "material-design-icons-iconfont/dist/material-design-icons.css";

Vue.use(Vuetify);
Vue.use(VueSocketio, "http://localhost:3090")
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
