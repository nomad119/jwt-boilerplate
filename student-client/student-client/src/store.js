import Vue from "vue";
import Vuex from 'vuex'
import * as api from './api'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    house: {},
    username: null,
    auth: false
  },
  getters: {
    HOUSE: state => {
      return state.house;
    },
    AUTH: state => {
      return state.auth
    }
  },
  mutations: {
    SET_HANDLE: (state, payload) => {
      state.handle = payload;
    },
    SET_HOUSE: (state, payload) =>{
      state.house = payload;
    },
    SET_USERNAME: (state, payload) =>{
      state.username = payload;
    }
  },
  actions: {
    SET_HANDLE: (context, payload) => {
      context.commit("SET_HANDLE", payload)
    },
    SET_HOUSE: context => {
      api.getHouse().then(house => {
        context.commit("SET_HOUSE", house);
      });
    },
    SET_USERNAME: context => {
      api.getStudentUsername().then(student =>{
        context.commit("SET_USERNAME", student);
      });
    },
    ADD_TASK: (context, payload) => {
      api.addTask(payload).then()
    },
    SET_AUTH: (context, payload) => {
      
    }
  }
})
