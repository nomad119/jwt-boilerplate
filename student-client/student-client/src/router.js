import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./components/Auth/Signin.vue";
import About from "./views/About.vue"
import {store} from './store';
import {mapGetters} from "vuex"
Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        requiredAuth: true
      },
    },
    {
      path: "/about",
      name: "about",
      component: About,
      meta: {
        requiredAuth: true
      },
    },{
      path: "/login",
      name: "login",
      component: Login
    }
  ]
});
router.beforeEach((to, from, next) => {
  console.log('checking routes')
  if (to.meta.requiredAuth) {
    if (store.getters.auth ) {
      next()
    } else {
      router.push('/login')
    }
  } else {
    next()
  }
})
export default router;
