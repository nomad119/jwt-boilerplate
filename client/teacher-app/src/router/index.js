import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Signin from '@/components/Auth/Signin'
import Students from '@/components/Students'
import store from '@/store'
import Houses from '@/components/Houses'
import Scoring from '@/components/Scoring'
import StudentHome from '@/components/StudentHome'
import Sorting from '@/components/Sorting'
import Onboard from '@/components/Onboard'
import ClassSelect from '@/components/ClassSelect'
import Settings from '@/components/Settings'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        requiredAuth: true
      },
      props: true
    },
    {
      path: '/login',
      name: 'signin',
      component: Signin
    },
    {
      path: '/students',
      name: 'Students',
      component: Students,
      meta: {
        requiredAuth: true
      },
      props: true
    },
    {
      path: '/houses',
      name: 'Houses',
      component: Houses,
      meta: {
        requiredAuth: true
      },
      props: true
    },
    {
      path: '/scoring',
      name: 'Scoring',
      component: Scoring,
      meta: {
        requiredAuth: true
      },
      props: true
    },
    {
      path: '/sorting',
      name: 'Sorting',
      component: Sorting,
      meta: {
        requiredAuth: true
      },
      props: true
    },
    {
      path: '/studentHome',
      name: 'StudentHome',
      component: StudentHome,
      meta: {
        requiredAuth: true
      },
      props: true
    },
    {
      path: '/onboard',
      name: 'Onboard',
      component: Onboard
    },
    {
      path: '/classSelect',
      name: 'ClassSelect',
      component: ClassSelect,
      props: true
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      meta: {
        requiredAuth: true
      }
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.meta.requiredAuth) {
    if (store.state.auth || localStorage.getItem('token')) {
      store.setAuth(true)
      next()
    } else {
      router.push('/login')
    }
  } else {
    next()
  }
})
export default router
