const store = {
  state: {
    auth: false,
    user: '',
    houses: [ ],
    students: [ ],
    points: [ ],
    classroom: {},
    userClass: [],
    housePoints: []
  },
  setAuth (auth) {
    this.state.auth = auth
  },
  setClassroom (classroom) {
    this.state.classroom = classroom
  },
  setHousePoints (housePoints) {
    this.state.housePoints = housePoints
  },
  setClasses (uClasses) {
    this.state.userClass = uClasses
  },
  setUsername (name) {
    this.state.user = name
  },
  setStudents (students) {
    this.state.students = students
  },
  setHouses (houses) {
    this.state.houses = houses
  },
  setPoints (points) {
    this.state.points = points
  },
  addStudent (student) {
    this.state.houses.map(house => {
      if (house.id === student.houseId) {
        Object.assign({'hName': house.hName}, student)
      }
    })
    this.state.students = [...this.state.students, student]
  }
}
export default store
