import Axios from 'axios'
import router from '@/router'
import store from '@/store'
const Sorting = 'http://localhost:3090'
export default {
  user: { authenticated: false },

  authenticate (context, credentials, redirect) {
    Axios.post(`${Sorting}/api/teachers/signin`, credentials)
        .then(({data}) => {
          /* context.$cookie.set('token', data.token, '1D')
          context.$cookie.set('user_id', data.user._id, '1D')
          context.validLogin = true
            */
          console.log(data)
          localStorage.setItem('token', data.token)
          this.user.authenticated = true
          store.setAuth(this.user.authenticated)
          console.log(data.name)
          store.setUsername({name: data.name, email: data.email})
          console.log()
          if (data.class.length !== 1) {
            store.setClasses(data.class)
            router.push({name: 'ClassSelect'})
          } else {
            store.setClassroom({cname: data.class[0].cname, urlId: data.class[0].urlId})
            localStorage.setItem('cname', data.class[0].cname)
            localStorage.setItem('urlId', data.class[0].urlId)
            if (redirect) router.push({name: redirect})
          }
        }).catch((response) => {
          context.snackbar = true
          context.message = 'Incorrect credentials.  Please try again.'
        })
  },

  signup (context, credentials, redirect) {
    Axios.post(`${Sorting}/api/teachers/signup`, credentials)
        .then(({data}) => {
          context.validSignUp = true
          localStorage.setItem('token', data.token)
          this.user.authenticated = true
          if (redirect) router.push(redirect)
          context.signSuccess = true
        }).catch(({response: {data}}) => {
          if (data.error){
            context.snackbar = true
            context.message = data.error
          } else {
            context.snackbar = true
            context.message = 'Network Error. Please contact Us'
          }
        })
  },

  signout (context, redirect) {
    context.$cookie.delete('token')
    context.$cookie.delete('user_id')
    this.user.authenticated = false

    if (redirect) router.push(redirect)
  },

  checkAuthentication () {
    const token = document.cookie
    this.user.authenticated = !!token
  },

  getAuthenticationHeader (context) {
    return `Bearer ${context.$cookie.get('token')}`
  }
}
