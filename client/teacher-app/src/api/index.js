import Axios from 'axios'
import store from '../store'
const Sorting = 'http://localhost:3090'
let classroom = store.state.classroom.urlId

if (!classroom) {
  classroom = localStorage.getItem('urlId')
}
console.log("asdfasdfklasdjgkljl;asdkgj;aklsdgja;sdgjkl;asdgjkl;asdgj;askld", classroom)
export function getUsername () {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = ''
    Axios.post(`${Sorting}/api/teacher`, data, axiosConfig)
      .then(({data}) => {
        store.setUsername({name: data.name, email: data.email})
        resolve(data)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
// Get all Houses by teacher ID through teacher token / id
export function getHouses () {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = {classroom}
    Axios.post(`${Sorting}/api/house/teach`, data, axiosConfig)
      .then(({ data }) => {
        resolve(store.setHouses(data.houses))
      })
      .catch((error) => {
        reject(console.log(error))
      })
  })
}
// Get all points by teacher ID through teacher token / id
export function getPoints () {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = {classroom}
    Axios.post(`${Sorting}/api/points`, data, axiosConfig)
      .then(({ data }) => {
        resolve(store.setPoints(data.points))
      })
      .catch((error) => {
        reject(console.log(error))
      })
  })
}
export function getPointsDate () {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = {classroom}
    Axios.post(`${Sorting}/api/points/date`, data, axiosConfig)
      .then(({ data }) => {
        resolve(store.setHousePoints(data.points))
      })
      .catch((error) => {
        reject(console.log(error))
      })
  })
}
// Get all students by teacher ID through teacher token / id
export function getStudents () {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = {classroom}
    Axios.post(`${Sorting}/api/student/teach`, data, axiosConfig)
    .then(({ data }) => {
      resolve(store.setStudents(data.students))
    })
    .catch((error) => {
      reject(console.log(error))
    })
  })
}
export function sortSelectedStudent (student) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = student
    Axios.post(`${Sorting}/api/students/sort`, data, axiosConfig)
      .then((data) => {
        resolve(data)
      })
      .catch((error) => {
        reject(console.log(error))
      })
  })
}
// Create new student
export function addStudent (newStudent) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, newStudent)
    Axios.post(`${Sorting}/api/students/add`, data, axiosConfig)
    .then(({ data }) => {
      getStudents()
      resolve(data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}
// Delete student
export function deleteStudent (student) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, student)
    Axios.post(`${Sorting}/api/student/del`, data, axiosConfig)
    .then(({data}) => {
      resolve(data)
    })
    .catch((error) => {
      console.log(error)
      reject(error)
    })
  })
}
// Edit student
export function editStudent (editStudent) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, editStudent)
    Axios.put(`${Sorting}/api/students/update`, data, axiosConfig)
    .then(({ data }) => {
      getStudents()
      resolve(data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}
// Create new house
export function addHouse (newHouse) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, newHouse)
    Axios.post(`${Sorting}/api/houses/add`, data, axiosConfig)
    .then(({ data }) => {
      getHouses()
      resolve(data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}
// Delete house by id
export function deleteHouse (house) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, house)
    Axios.post(`${Sorting}/api/house/del`, data, axiosConfig)
      .then(({data}) => {
        resolve(data)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
// Edit house
export function editHouse (editHouse) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, editHouse)
    Axios.put(`${Sorting}/api/house/update`, data, axiosConfig)
    .then(({ data }) => {
      getHouses()
      resolve(data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}
export function addPoint (newPoint) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, newPoint)
    console.log('asdfasdfasdf', data)
    Axios.post(`${Sorting}/api/points/add`, data, axiosConfig)
    .then(({ data }) => {
      getPoints()
      resolve(data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}
// Delete student
export function deletePoint (point) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, point)
    Axios.post(`${Sorting}/api/point/del`, data, axiosConfig)
    .then(({data}) => {
      resolve(data)
    })
    .catch((error) => {
      console.log(error)
      reject(error)
    })
  })
}
// Edit point
export function editPoint (editPoint) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = Object.assign({classroom}, editPoint)
    Axios.put(`${Sorting}/api/point/update`, data, axiosConfig)
    .then(({ data }) => {
      getPoints()
      resolve(data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}

export function createClassroom (newClass) {
  return new Promise((resolve, reject) => {
    let axiosConfig = { headers: { authorization: localStorage.getItem('token') } }
    let data = newClass
    Axios.post(`${Sorting}/api/classroom/add`, data, axiosConfig)
    .then(({data}) => {
      // Should return unqiue classroom id to share with others for the classroom.
      resolve(data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}
